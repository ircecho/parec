import math
import os
import random
from typing import Union

import pandas as pd
from keras.callbacks import EarlyStopping

import autoencoder

imputed = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')

species = [(x, y, z) for x in ['relu', 'tanh'] for y in ['relu', 'tanh'] for z in ['Adagrad', 'RMSprop', 'Adam']]
search_space = ['latent_size', 'beta', 'no_of_encoder_layers', 'no_of_decoder_layers', 'size_intermediate_layers']


def generate(name: int) -> pd.Series:
    spec = species[name % len(species)]

    individual: pd.Series = pd.Series(name=name)

    individual['latent_size'] = random.uniform(round(37 * 0.8), round(37 * 1.2))  #
    individual['beta'] = random.expovariate(1 / 50)  #
    individual['no_of_encoder_layers'] = random.uniform(1, 6)  #
    individual['no_of_decoder_layers'] = random.uniform(1, 6)  #
    individual['size_intermediate_layers'] = random.expovariate(1 / 309)  #
    individual['encoder_activation']: str = spec[0]
    individual['decoder_activation']: str = spec[1]
    individual['optimizer'] = spec[2]
    individual["mutation_rate"] = 0.2

    return individual


def mutate(individual: pd.Series, new_name: int) -> pd.Series:
    tau = 1 / math.sqrt(len(search_space))

    individual = individual.drop('inverse_loss')
    individual["parent"] = individual.name
    individual.name = new_name

    individual["mutation_rate"] *= math.exp(tau * random.normalvariate(0, 1))

    for ind in search_space:
        individual[ind] *= random.normalvariate(1, individual["mutation_rate"])

    return individual


def train(individual: pd.Series):
    if not (37 * 0.8 < individual["latent_size"] < 37 * 1.2):
        return None, None, 0
    if not (1 < individual["beta"]):
        return None, None, 0
    if not (1 <= round(individual['no_of_encoder_layers'])):
        return None, None, 0
    if not (1 <= round(individual['no_of_decoder_layers'])):
        return None, None, 0
    if not (1 <= round(individual['size_intermediate_layers'])):
        return None, None, 0

    parameters: autoencoder.AutoEncoderParameters = autoencoder.AutoEncoderParameters()
    parameters.latent_size = round(individual["latent_size"])
    parameters.beta = individual["beta"]
    parameters.no_of_encoder_layers = round(individual['no_of_encoder_layers'])
    parameters.no_of_decoder_layers = round(individual['no_of_decoder_layers'])
    parameters.size_intermediate_layers = round(individual['size_intermediate_layers'])
    parameters.encoder_activation = individual['encoder_activation']
    parameters.decoder_activation = individual['decoder_activation']
    parameters.optimizer = individual['optimizer']

    network = autoencoder.build_autoencoder(parameters)
    early_stopping: EarlyStopping = EarlyStopping(monitor='loss', min_delta=0.001, patience=10, verbose=1, mode='min')

    history = network.ae.fit(imputed, imputed, epochs=10000, callbacks=[early_stopping], verbose=2)

    quality = 1 / min(history.history['loss'])
    return network, parameters, quality


def run(pop: pd.DataFrame, new_index: int, pop_folder: str, pop_file: str) -> Union[pd.DataFrame, None]:
    if new_index in pop.index:
        return pop

    if new_index < 800:
        individual: pd.Series = generate(new_index)
    else:
        # Fallback setting. If the following method fails for any reason, we already have an individual.
        individual: pd.Series = pop.sample().iloc[0]

        parents = pop.sort_values('inverse_loss', ascending=False).head(800)

        rand = random.uniform(0, parents['inverse_loss'].sum())
        running_total = 0
        for ind, candidate in parents.iterrows():
            # For NaN candidates
            if not (candidate['inverse_loss'] > 0):
                continue

            running_total += candidate['inverse_loss']
            individual = candidate
            if running_total >= rand:
                break

        individual: pd.Series = mutate(individual, new_index)

    print("Training:")
    print(individual)
    network, parameters, inverse_loss = train(individual)

    if network is None:
        print("Unviable individual!")
        return None

    individual["inverse_loss"] = inverse_loss
    print("Loss ", 1 / inverse_loss)

    parameters.to_file(f"{pop_folder}/{new_index}_parameters.py")
    network.ae.save_weights(f"{pop_folder}/{new_index}_weights.hdf")

    if pop.empty or pop['inverse_loss'].max() < inverse_loss:
        parameters.to_file(f"output/best_parameters.py")
        network.ae.save_weights(f"output/best_weights.hdf")

    pop = pop.append(individual)
    pop.sort_index(inplace=True)
    pop.to_csv(pop_file)

    print(pop.sort_values('inverse_loss', ascending=False).head())

    return pop


def train_models():
    population_file = f"output/ae-population.csv"
    population_folder = f"/home/ircecho/ae-population/"

    os.makedirs(population_folder, exist_ok=True)
    os.makedirs("output", exist_ok=True)

    if os.path.isfile(population_file):
        pop: pd.DataFrame = pd.read_csv(population_file, index_col='index')
    else:
        pop: pd.DataFrame = pd.DataFrame(index=pd.Series(name="index"))

    for i in range(8 * 1000):
        new_pop = None
        while new_pop is None:
            new_pop = run(pop, i, population_folder, population_file)
        pop = new_pop


train_models()
