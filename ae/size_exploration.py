import pandas as pd
from keras.callbacks import EarlyStopping

import autoencoder
import output.second_parameters

imputed = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')


def cluster(start: int, stop: int, output_suffix=""):
    try:
        size_table = pd.read_csv(f'output/size_exploration_{output_suffix}.csv', index_col='latent_size')
    except FileNotFoundError:
        size_table = pd.DataFrame()
        size_table.index.name = 'latent_size'

    for latent_size in range(start, stop + 1):
        for i in range(10):
            if 10 * latent_size + i in size_table.index:
                continue

            parameters = output.second_parameters.build()
            parameters.latent_size = latent_size
            ae_instance = autoencoder.build_autoencoder(parameters)

            # Train to perfection.

            early_stopping: EarlyStopping = EarlyStopping(monitor='loss', min_delta=0.001, patience=10, verbose=1,
                                                          mode='min')

            history = ae_instance.ae.fit(imputed, imputed, epochs=10000, callbacks=[early_stopping], verbose=2)

            loss = min(history.history['loss'])

            if 'loss' not in size_table.columns or loss < size_table['loss'].min():
                print("New record: " + str(loss))
                parameters.to_file(f"output/best_parameters_{output_suffix}.py")
                ae_instance.ae.save_weights(f"output/best_weights_{output_suffix}.hdf")

            size_table.at[10 * latent_size + i, 'loss'] = loss

            size_table.to_csv(f'output/size_exploration_{output_suffix}.csv')


for band in [(7, 14), (15, 21), (22, 29)]:
    cluster(band[0], band[1], f"-{band[0]}-{band[1]}")
