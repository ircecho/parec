import os
import re

import numpy as np
import pandas as pd
from pandas import DataFrame

import autoencoder
import output.best_parameters_22_29
import output.second_parameters

imputed = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')

# models = [  # ("output/5842_weights.hdf", output.second_parameters.build(), "ae.csv"),
# ("output/best_weights_-7-14.hdf", output.best_parameters_7_14.build(), "ae-size-exploration/ae-7-14.csv"),
# ("output/best_weights_-15-21.hdf", output.best_parameters_15_21.build(), "ae-size-exploration/ae-15-21.csv"),
# ("output/best_weights_-22-29.hdf", output.best_parameters_22_29.build(), "ae-size-exploration/ae-22-29.csv")]

parameters = output.best_parameters_22_29.build()
weights = "output/best_weights_-22-29-fully-trained.hdf"
output_name = "ae-size-exploration/ae-22-29.csv"

ae_instance = autoencoder.build_autoencoder(parameters)
ae_instance.ae.load_weights(weights)

latent_representation = pd.DataFrame(index=imputed.index, data=ae_instance.encoder.predict(imputed))
latent_representation.to_csv("../clustering/representations/" + output_name)


os.makedirs("output/evaluation", exist_ok=True)

latent_describe = latent_representation.describe()
latent_describe.to_csv('output/evaluation/desc.csv', float_format='%.2f')

reconstruction = pd.DataFrame(index=imputed.index, columns=imputed.columns,
                              data=ae_instance.ae.predict(imputed))
reconstruction.to_csv("output/evaluation/reconstruction.csv", float_format='%.2f')

difference = (reconstruction - imputed).abs()
difference.to_csv("output/evaluation/difference.csv", float_format='%.2f')


# #### EVALUATION ####


def calculate_differences(stddev_multiplier: float):
    print("Calculating differences " + str(stddev_multiplier) + " * sigma")
    results_array = []
    for j in range(latent_representation.shape[1]):
        print(f"Parameter {j}")

        new_reps = latent_representation.copy()
        new_reps[j] += stddev_multiplier * latent_describe[j].loc['std']

        new_recs = ae_instance.decoder.predict(new_reps)

        results_array.append((new_recs - reconstruction).sum().tolist())

    return DataFrame(data=results_array, columns=imputed.columns)


os.makedirs("output/evaluation/influence-tables", exist_ok=True)

sort_order = None

for stddev in [-2,2]:
    full_table = calculate_differences(stddev)

    value_array = full_table.abs().to_numpy().flatten()
    value_array.sort()
    cut_off = value_array[-len(full_table.index) * 7]
    cut_off2 = value_array[int((-len(full_table.index) * 7) / 2.0)]

    with open(f"output/evaluation/influence-tables/{stddev}-stddev-hr.txt", "w") as text_file:

        for ind, row in full_table.iterrows():
            sorted_row = row.sort_values()
            strings = []
            for ind2, val in sorted_row.iteritems():
                if abs(val) >= cut_off2:
                    if val > 0:
                        strings.append("++" + ind2)
                    elif val < 0:
                        strings.append("--" + ind2)
                elif abs(val) >= cut_off:
                    if val > 0:
                        strings.append("+" + ind2)
                    elif val < 0:
                        strings.append("-" + ind2)

            print(ind, ": ", "; ".join(strings), sep='', file=text_file)

    with open(f"output/evaluation/influence-tables/{stddev}-stddev-hr2.txt", "w") as text_file:

        for ind, row in full_table.iterrows():
            sorted_row = row.abs().sort_values(ascending=False)
            strings = []
            for ind2, val in sorted_row.head(7).iteritems():
                strings.append(f"{round(row[ind2])}*{ind2}")

            print(ind, ": ", "; ".join(strings), sep='', file=text_file)

    with open(f"output/evaluation/influence-tables/{stddev}-stddev-hr3.txt", "w") as text_file:
        median = np.median(full_table.abs().values)

        result = [[] for i in full_table.index]
        for col in full_table:
            l_uns = list(enumerate(full_table[col].tolist()))
            l_sort = list(sorted(filter(lambda f: abs(f[1]) > median, l_uns), key=lambda f: -abs(f[1])))

            if len(l_sort) > 0:
                result[l_sort[0][0]].append(col + ("++" if l_sort[0][0] > 0 else "--"))
            if len(l_sort) > 1:
                result[l_sort[1][0]].append(col + ("+" if l_sort[1][0] > 0 else "-"))

        if sort_order is None:
            sort_order = list(map(lambda f: f[0], sorted(enumerate(full_table.abs().sum(axis=1).tolist()), key=lambda f: -f[1])))

        for ind, arr in enumerate(map(lambda s: result[s], sort_order)):
            joined = ' \\\\ '.join(arr).replace('_', '\\_')
            sep = " & " if ind % 2 < 1 else " \\\\ \\hline"
            print(f"\\makecell{{\\textbf{{Attribute {ind + 1}}} \\\\ {joined} }}{sep} % {sort_order[ind]}", file=text_file)

    with open(f"output/evaluation/influence-tables/{stddev}-stddev-hr4.txt", "w") as text_file:
        median = np.median(full_table.abs().values)

        result = [[] for i in full_table.index]
        for col in full_table:
            l_uns = list(enumerate(full_table[col].tolist()))
            l_sort = list(sorted(filter(lambda f: abs(f[1]) > median, l_uns), key=lambda f: -abs(f[1])))

            if len(l_sort) > 0:
                result[l_sort[0][0]].append(col + ("+" if l_sort[0][0] > 0 else "-"))

        print("\\begin{figure}", file=text_file)
        print("\t\\begin{tiny}", file=text_file)
        print("\t\t\\begin{description}", file=text_file)
        print("\t\t\t\\setlength\\itemsep{0.5ex}", file=text_file)
        for ind, line in enumerate(map(lambda s: result[s], sort_order)):
            attributes = ", ".join(map(lambda att: f"\\att{{{att}}}", line))
            print("\t\t\t\\item[Attribute ", ind + 1, "] ", attributes, sep='', file=text_file)
        print("\t\t\\end{description}", file=text_file)
        print("\t\\end{tiny}", file=text_file)
        print("\t\\caption{Influence of a $" + str(stddev) + "\sigma$ adjustment to each attribute of the representation.}", file=text_file)
        print("\t\\label{fig:" + str(stddev) + "-sigma-influence}", file=text_file)
        print("\\end{figure}", file=text_file)


    full_table.to_csv(f"output/evaluation/influence-tables/{stddev}-stddev.csv", float_format="%.2f")
    full_table.transpose().to_csv(f"output/evaluation/influence-tables/{stddev}-stddev-transposed.csv",
                                  float_format="%.2f")
