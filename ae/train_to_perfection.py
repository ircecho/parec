import os

import pandas as pd
from keras.callbacks import EarlyStopping

import autoencoder
import output.best_parameters_22_29
import output.second_parameters

imputed = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')


def train_to_perfection(weights, parameters):
    instances = []

    for i in range(10):
        ae_instance = autoencoder.build_autoencoder(parameters)
        ae_instance.ae.load_weights(weights)
        # Train to perfection.
        early_stopping: EarlyStopping = EarlyStopping(monitor='loss', patience=50, verbose=1, mode='min',
                                                      restore_best_weights=True)
        history = ae_instance.ae.fit(imputed, imputed, epochs=10000, callbacks=[early_stopping], verbose=2)
        loss = min(history.history['loss'])

        instances.append((ae_instance, loss))

    top_candidate = min(instances, key=lambda item: item[1])

    top_candidate[0].ae.save_weights(os.path.splitext(weights)[0] + "-fully-trained.hdf")
    print(top_candidate[1])


train_to_perfection("output/best_weights_-22-29.hdf", output.best_parameters_22_29.build())
