from keras import Input
from keras.layers import (Conv2D, BatchNormalization, LeakyReLU, Dense)

from keras.models import Model
from tensorflow import keras


class ConvBnLRelu(object):
    def __init__(self, filters, kernel_size, strides=1):
        self.filters = filters
        self.kernelSize = kernel_size
        self.strides = strides

    # return conv + bn + leaky_relu model
    def __call__(self, net, training=None):
        net = Conv2D(self.filters, self.kernelSize, strides=self.strides, padding='same')(net)
        net = BatchNormalization()(net, training=training)
        net = LeakyReLU()(net)
        return net


class AutoEncoder(object):
    def __init__(self, encoder_architecture,
                 decoder_architecture):
        self.encoder = encoder_architecture.model
        self.decoder = decoder_architecture.model

        self.ae = Model(self.encoder.inputs, self.decoder(self.encoder.outputs))


class Architecture(object):
    '''
    generic architecture template
    '''

    def __init__(self, inputShape=None, batchSize=None, latentSize=None):
        '''
        params:
        ---------
        inputShape : tuple
            the shape of the input, expecting 3-dim images (h, w, 3)
        batchSize : int
            the number of samples in a batch
        latentSize : int
            the number of dimensions in the two output distribution vectors -
            mean and std-deviation
        latentSize : Bool or None
            True forces resampling, False forces no resampling, None chooses based on K.learning_phase()
        '''
        self.inputShape = inputShape
        self.batchSize = batchSize
        self.latentSize = latentSize

        self.model = self.Build()

    def Build(self):
        raise NotImplementedError('architecture must implement Build function')


class Darknet19Encoder(Architecture):
    '''
    This encoder predicts distributions then randomly samples them.
    Regularization may be applied to the latent space output

    a simple, fully convolutional architecture inspried by
        pjreddie's darknet architecture
    https://github.com/pjreddie/darknet/blob/master/cfg/darknet19.cfg
    '''

    def __init__(self, inputShape=(274,), batchSize=None, latentSize=37, latentConstraints='bvae', beta=100.,
                 training=None, layerNo=3, sizeOfIntermediateLayers=274, activation='tanh'):
        '''
        params
        -------
        latentConstraints : str
            Either 'bvae', 'vae', or 'no'
            Determines whether regularization is applied
                to the latent space representation.
        beta : float
            beta > 1, used for 'bvae' latent_regularizer
            (Unused if 'bvae' not selected, default 100)
            :param activation:
            :param sizeOfIntermediateLayers:
            :param layerNo:
        '''
        self.latentConstraints = latentConstraints
        self.beta = beta
        self.training = training
        self.layerNo = layerNo
        self.sizeOfIntermediateLayers = sizeOfIntermediateLayers
        self.activation = activation
        super().__init__(inputShape, batchSize, latentSize)

    def Build(self):
        # create the input layer for feeding the netowrk
        inLayer = Input(self.inputShape, self.batchSize)
        net = inLayer

        for l in range(0, self.layerNo - 1):
            net = Dense(self.sizeOfIntermediateLayers, activation=self.activation)(net)  # 1

        # variational encoder output (distributions)
        mean = Dense(units=self.latentSize)(net)
        logvar = Dense(units=self.latentSize)(net)

        sample = SampleLayer(self.latentConstraints, self.beta)([mean, logvar], training=self.training)

        return Model(inputs=inLayer, outputs=sample)


class Darknet19Decoder(Architecture):
    def __init__(self, inputShape=(274,), batchSize=None, latentSize=37, training=None, layerNo=3,
                 sizeOfIntermediateLayers=274, activation='tanh'):
        self.training = training
        self.layerNo = layerNo
        self.sizeOfIntermediateLayers = sizeOfIntermediateLayers
        self.activation = activation

        super().__init__(inputShape, batchSize, latentSize)

    def Build(self):
        inLayer = Input([self.latentSize], self.batchSize)
        net = inLayer
        for l in range(0, self.layerNo - 1):
            net = Dense(self.sizeOfIntermediateLayers, activation=self.activation)(net)  # 1

        net = Dense(274, activation=self.activation)(net)  # 1

        return Model(inLayer, net)


'''
sample_layer.py
contains keras SampleLayer for bvae

created by shadySource

THE UNLICENSE
'''

from keras.layers import Layer
from keras import backend as K


class SampleLayer(Layer):
    '''
    Keras Layer to grab a random sample from a distribution (by multiplication)
    Computes "(normal)*logvar + mean" for the vae sampling operation
    (written for tf backend)

    Additionally,
        Applies regularization to the latent space representation.
        Can perform standard regularization or B-VAE regularization.

    call:
        pass in mean then logvar layers to sample from the distribution
        ex.
            sample = SampleLayer('bvae', 16)([mean, logvar])
    '''

    def __init__(self, latent_regularizer='bvae', beta=100., **kwargs):
        '''
        args:
        ------
        latent_regularizer : str
            Either 'bvae', 'vae', or 'no'
            Determines whether regularization is applied
                to the latent space representation.
        beta : float
            beta > 1, used for 'bvae' latent_regularizer,
            (Unused if 'bvae' not selected)
        ------
        ex.
            sample = SampleLayer('bvae', 16)([mean, logvar])
        '''
        if latent_regularizer.lower() in ['bvae', 'vae']:
            self.reg = latent_regularizer
        else:
            self.reg = None

        if self.reg == 'bvae':
            self.beta = beta
        elif self.reg == 'vae':
            self.beta = 1.

        super(SampleLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # save the shape for distribution sampling
        super(SampleLayer, self).build(input_shape)  # needed for layers

    def call(self, x, training=None):
        if len(x) != 2:
            raise Exception('input layers must be a list: mean and logvar')
        if len(x[0].shape) != 2 or len(x[1].shape) != 2:
            raise Exception('input shape is not a vector [batchSize, latentSize]')

        mean = x[0]
        logvar = x[1]

        # trick to allow setting batch at train/eval time
        if mean.shape[0] is None or mean.shape[0].value is None or logvar.shape[0].value is None or logvar.shape[
            0] is None:
            return mean + 0 * logvar  # Keras needs the *0 so the gradinent is not None

        if self.reg is not None:
            # kl divergence:
            latent_loss = -0.5 * (1 + logvar
                                  - K.square(mean)
                                  - K.exp(logvar))
            latent_loss = K.sum(latent_loss, axis=-1)  # sum over latent dimension
            latent_loss = K.mean(latent_loss, axis=0)  # avg over batch

            # use beta to force less usage of vector space:
            latent_loss = self.beta * latent_loss
            self.add_loss(latent_loss, x)

        def reparameterization_trick():
            epsilon = K.random_normal(shape=logvar.shape)
            stddev = K.exp(logvar * 0.5)
            return mean + stddev * epsilon

        return K.in_train_phase(reparameterization_trick, mean + 0 * logvar, training=training)

    def compute_output_shape(self, input_shape):
        return input_shape[0]


class AutoEncoderParameters:
    latent_size = 39  #
    beta = 60  #
    no_of_encoder_layers = 1  #
    no_of_decoder_layers = 3  #
    size_intermediate_layers = 1066  #
    encoder_activation: str = 'relu'
    decoder_activation: str = 'relu'
    optimizer = keras.optimizers.RMSprop()

    def to_string(self):
        return f"{self.latent_size},{self.beta},{self.no_of_encoder_layers},{self.no_of_decoder_layers},{self.size_intermediate_layers},{self.encoder_activation},{self.decoder_activation},{self.optimizer.__class__.__name__}"

    @staticmethod
    def to_header():
        return "{latent_size},{beta},{no_of_encoder_layers},{no_of_decoder_layers},{size_intermediate_layers},{encoder_activation},{decoder_activation},{optimizer.__class__.__name__}"

    def to_file(self, path: str):
        with open(path, "w") as f:
            f.write('from autoencoder import AutoEncoderParameters\n')
            f.write('\n')
            f.write('\n')
            f.write('def build() -> AutoEncoderParameters:\n')
            f.write('\tparameters: AutoEncoderParameters = AutoEncoderParameters()\n')
            f.write("\tparameters.latent_size = " + str(self.latent_size) + "\n")
            f.write("\tparameters.beta = " + str(self.beta) + "\n")
            f.write("\tparameters.no_of_encoder_layers = " + str(self.no_of_encoder_layers) + "\n")
            f.write("\tparameters.no_of_decoder_layers = " + str(self.no_of_decoder_layers) + "\n")
            f.write("\tparameters.size_intermediate_layers = " + str(self.size_intermediate_layers) + "\n")
            f.write("\tparameters.encoder_activation = '" + str(self.encoder_activation) + "'\n")
            f.write("\tparameters.decoder_activation = '" + str(self.decoder_activation) + "'\n")
            f.write("\tparameters.optimizer = '" + str(self.optimizer) + "'\n")
            f.write("\treturn parameters\n")


def build_autoencoder(parameters: AutoEncoderParameters) -> AutoEncoder:
    encoder = Darknet19Encoder(latentSize=parameters.latent_size, beta=parameters.beta,
                               layerNo=parameters.no_of_encoder_layers,
                               sizeOfIntermediateLayers=parameters.size_intermediate_layers,
                               activation=parameters.encoder_activation)
    decoder = Darknet19Decoder(latentSize=parameters.latent_size, layerNo=parameters.no_of_decoder_layers,
                               sizeOfIntermediateLayers=parameters.size_intermediate_layers,
                               activation=parameters.decoder_activation)
    bvae = AutoEncoder(encoder, decoder)
    bvae.ae.compile(optimizer=parameters.optimizer, loss='mean_absolute_error')
    return bvae
