import math
import os

import pandas as pd
import numpy as np
from osgeo import gdal


def read_giata_file(path: str, dtype) -> pd.DataFrame:
    return pd.read_csv(path,
                       sep=';',
                       index_col=0,
                       encoding='iso-8859-15',
                       decimal=",",
                       dtype=dtype,
                       engine="python")


giata_facts_used = read_giata_file('../../giata_facts_used.csv', {'category_official': float})
giata_facts_used_desc = giata_facts_used.describe(percentiles=[.1, .9])
offerte_categories_giata = read_giata_file('../../offerte_categories_giata.csv', {})


def sanitize_coordinates(giata_id: int, offerte: pd.DataFrame):
    if giata_id == 103492:
        return 9.500738, 100.057233
    if giata_id == 111685:
        return 36.401736, 25.477790
    if giata_id == 11223:
        return 11.26202, 43.781
    if giata_id == 11303:
        return 40.183081, 17.47369
    if giata_id == 125718:
        return 47.551, 13.51598
    if giata_id == 126651:
        return 47.76105, 11.559
    if giata_id == 33014:
        return 45.678, 13.38478
    if giata_id == 33282:
        return 45.243, 13.60238
    if giata_id == 702:
        return 36.89726, 27.246
    if giata_id == 796:
        return 35.340112, 25.174357
    if giata_id == 33615:
        return 46.669, 11.59982
    if giata_id == 1297:
        return 36.24777, 28.164
    if giata_id == 231858:
        return 45.324, 13.56339
    if giata_id == 2830:
        return 38.992, 1.29085
    if giata_id == 36195:
        return 46.63409, 14.144
    if giata_id == 232821:
        return 43.69, 13.265
    if giata_id == 265622:
        return 45.637, 10.61243
    if giata_id == 36569:
        return 47.189, 13.964
    if giata_id == 201356:
        return 54.384224, 12.4257919
    if giata_id == 430775:
        return 45.483, 13.49811
    if giata_id == 70343:
        return 25.781270000000003, -80.265
    if giata_id == 562458:
        return -8.48986, 115.263
    if giata_id == 6721:
        return 45.237, 13.598460000000001
    if giata_id == 203427:
        return 46.502, 11.419939999999999
    if giata_id == 400800:
        return 45.68722, 13.129
    if giata_id == 7994:
        return 35.340128, 25.174671
    if giata_id == 41024:
        return 39.25464, 9.575
    if giata_id == 238050:
        return 47.74191, 9.223
    if giata_id == 8760:
        return 45.6337, 13.052
    if giata_id == 139856:
        return 45.05919, 13.624
    if giata_id == 75295:
        return 41.37682, 2.146
    if giata_id == 75639:
        return 36.400788, 25.477412
    if giata_id == 76551:
        return 46.782, 9.679739999999999
    if giata_id == 77086:
        return 50.94804, 7.006
    if giata_id == 241553:
        return 45.46689, 10.604
    if giata_id == 12861:
        return 46.783333, 11.233333
    if giata_id == 78630:
        return 27.1002, 33.828
    if giata_id == 13802:
        return 47.448, 12.10018
    if giata_id == 13961:
        return 35.293, 25.46184
    if giata_id == 472868:
        return 58.395680000000006, 15.086
    if giata_id == 47210:
        return 45.802, 9.09003
    if giata_id == 15072:
        return 35.339892, 25.174561
    if giata_id == 15435:
        return 49.26356, 12.968
    if giata_id == 15632:
        return 45.787, 10.76287
    if giata_id == 15850:
        return 36.800960, 27.087674
    if giata_id == 16020:
        return 46.486, 9.8372
    if giata_id == 17271:
        return 47.076, 15.429879999999999
    if giata_id == 214238:
        return 45.546477, 10.564031
    if giata_id == 182440:
        return 46.723, 14.045320000000002
    if giata_id == 183011:
        return 47.563, 13.649020000000002
    if giata_id == 412399:
        return 46.372, -1.41457
    if giata_id == 51981:
        return 59.918519999999994, 10.732
    if giata_id == 19651:
        return 46.747091, 11.206265
    if giata_id == 85391:
        return 40.278, 9.62693
    if giata_id == 85759:
        return 43.913, 15.497010000000001
    if giata_id == 413824:
        return 35.339779, 25.174337
    if giata_id == 86291:
        return 45.764520000000005, 10.808
    if giata_id == 53603:
        return 36.808, 31.340729999999997
    if giata_id == 414160:
        return 43.96709, 12.742
    if giata_id == 21690:
        return 45.443, 13.5106
    if giata_id == 55679:
        return 46.83715, 13.369
    if giata_id == 23126:
        return 47.023, 10.742469999999999
    if giata_id == 56021:
        return 46.38005, 11.252
    if giata_id == 56838:
        return 47.32284, 9.976
    if giata_id == 24430:
        return 51.525, -0.14373
    if giata_id == 25027:
        return 47.768, 12.75078
    if giata_id == 25249:
        return 47.129, 10.26543
    if giata_id == 59553:
        return 46.372, 14.107420000000001
    if giata_id == 92751:
        return 47.14115, 9.778
    if giata_id == 27617:
        return 47.687, 11.57259
    if giata_id == 650975:
        return 40.771, 9.666210000000001
    if giata_id == 95094:
        return 54.10363, 11.903
    if giata_id == 160822:
        return 46.686, 11.190560000000001
    if giata_id == 95321:
        return 45.692, 13.14814
    if giata_id == 292133:
        return 46.371, 10.69165
    if giata_id == 259780:
        return 52.36813, 4.844
    if giata_id == 30497:
        return 47.436, 12.31853
    if giata_id == 260002:
        return 36.800960, 27.087684
    if giata_id == 128959:
        return 46.967, 11.92567
    if giata_id == 391238:
        return 47.37982, 12.596
    if giata_id == 31002:
        return 47.449939, 12.158713
    if giata_id == 97218:
        return 44.551, 14.883070000000002
    if giata_id == 31695:
        return 50.880956, 7.1074932
    if giata_id == 31741:
        return -36.85622, 174.787
    if giata_id == 97537:
        return 38.796, 20.71681
    if giata_id == 130810:
        return 48.698, 9.01914
    if giata_id == 32736:
        return 45.399229999999996, 11.016
    if giata_id == 11223:
        return 	43.781, 11.26202

    lats = []
    lons = []
    for ind, row in offerte.iterrows():
        u_lat, u_lon = row['ADR_MAP_Y'], row['ADR_MAP_X']

        if math.isnan(u_lat) or math.isnan(u_lon):
            continue
        if u_lat < -90 or u_lat > 90 or round(u_lat) == u_lat:
            continue
        if u_lon < -180 or u_lon > 180 or round(u_lat) == u_lat:
            continue

        lats.append(u_lat)
        lons.append(u_lon)

    if not lats:
        row = offerte.sample().iloc(0)
        print(f"if giata_id == {giata_id}: return {row['ADR_MAP_Y']}, {row['ADR_MAP_X']} ({row['PRD_BEZ']},"
              f" {row['ADR_PLZ']} {row['GEO_BEZ']})")
        raise Exception("Failure to find coordinates.")

    return sum(lats) / len(lats), sum(lons) / len(lons)


def load_offers():
    res_offerte = pd.DataFrame()

    for giata_id in set(offerte_categories_giata['DBU_GIATA_ID'].to_list()):
        if math.isnan(giata_id):
            continue
        giata_id = int(giata_id)

        offerte = offerte_categories_giata.loc[offerte_categories_giata['DBU_GIATA_ID'] == giata_id]

        lat, lon = sanitize_coordinates(giata_id, offerte)
        res_offerte.at[giata_id, 'Latitude'] = lat
        res_offerte.at[giata_id, 'Longitude'] = lon

        for col_name in ["PRD_SW_PKW", "PRD_SW_BUS", "PRD_SW_SCHIFF", "PRD_SW_FLUG", "PRD_SW_RUND", "PRD_SW_FERN",
                         "PRD_SW_SPORT", "PRD_SW_WELL", "PRD_SW_STADT", "PRD_SW_EVENT", "PRD_SW_PARK", "PRD_SW_FAM",
                         "PRD_SW_PARTNER", "PRD_SW_SINGLE", "PRD_SW_FREUND", "PRD_SW_STORNO", "PRD_SW_WASSER",
                         "PRD_SW_BERG", "PRD_SW_SONDER", "PRD_SW_BAHN", "PRD_SW_AKTIV", "PRD_SW_EK", "PRD_SW_E2C_SPLIT",
                         "kreuz_schiff_reise", "wellness_urlaub", "familien_urlaub", "event_reise", "gruppenreise",
                         "individualreise", "rund_reise"]:
            res_offerte.at[giata_id, col_name] = offerte[col_name].max()

        for col_name in ["Berge", "Meer_Kueste_Strand", "Seen", "Stadt", "Natur_Landschaft", "warm", "kalt",
                         "sonnig_heiß",
                         "mild",
                         "kuehl", "Wellness_Spas", "Kunst._Kultur", "Sehenswuerdigkeiten", "Gastronomie", "Nightlife",
                         "History",
                         "Touren", "Maerkte", "Unterhaltungsevents", "Wintersport", "Sommersport", "Extremsport",
                         "Bewegung_und_Sport", "Essen", "Trinken", "Sightseeing", "Shopping", "Entertainment",
                         "Spazieren_Wanderm",
                         "Erholungsaktiviäten", "Natur_beobachten", "Kurse_Workshops", "Kulturaktivitaeten",
                         "relaxing_recreative",
                         "family_friendly", "exciting_thrilling", "entertaining", "calming", "exclusive_gehoben",
                         "alternativ_guenstig", "Adventure", "Romantik"]:
            res_offerte.at[giata_id, col_name] = offerte[col_name].max()

        random_offert = offerte.sample().iloc[0]
        for col_name in ["PRD_BEZ", "ADR_STRASSE", "ADR_PLZ", "GEO_BEZ", "DBU_LNDCODE"]:
            res_offerte.at[giata_id, col_name] = random_offert[col_name]

    res_offerte_desc = res_offerte.describe(percentiles=[.1, .9])

    return res_offerte, res_offerte_desc


grouped_offers, grouped_offers_desc = load_offers()


def min_max_scaler(value, desc, c_name):
    return (value - desc[c_name]['min']) / (desc[c_name]['max'] - desc[c_name]['min'])



def distance_scaler(value):
    if value == 1:
        return 1
    return 8 / math.log(value) - 0.65


def mean_imputer(value, desc, c_name):
    if math.isnan(value):
        return desc[c_name]['mean']
    else:
        return value


def zero_imputer(value):
    if math.isnan(value):
        return 0
    else:
        return value


def force_01(value):
    if value < 0:
        return 0
    if value > 1:
        return 1
    else:
        return value


imputed = pd.DataFrame()
imputed.index.name = "LOB_GIATA"

hotel_information = pd.DataFrame()
hotel_information.index.name = "LOB_GIATA"


def load_tiff():
    filename = "input/Beck_KG_V1_present_0p0083.tif"  # path to raster
    dataset = gdal.Open(filename)
    band = dataset.GetRasterBand(1)
    cols = dataset.RasterXSize
    rows = dataset.RasterYSize
    transform = dataset.GetGeoTransform()
    x_origin = transform[0]
    y_origin = transform[3]
    pixel_width = transform[1]
    pixel_height = -transform[5]
    data = band.ReadAsArray(0, 0, cols, rows)

    return x_origin, y_origin, pixel_width, pixel_height, data


tifxOrigin, tifyOrigin, tifpixelWidth, tifpixelHeight, tifdata = load_tiff()


def get_climate2(lat, lon):
    if math.isnan(lat) or math.isnan(lon):
        return 0

    tif_col = int((lon - tifxOrigin) / tifpixelWidth)
    tif_row = int((tifyOrigin - lat) / tifpixelHeight)

    # This is for coast regions, where data for climate over the ocean is not available and some hotels, due to
    # calculation inaccuracies happen to be over the water, and would otherwise not be found
    for x, y in sorted([(x, y) for x in range(-1, 2) for y in range(-1, 2)], key=lambda c: c[0] * c[0] + c[1] * c[1]):
        try:
            if tifdata[tif_row + x][tif_col + y] > 0:
                return tifdata[tif_row + x][tif_col + y]
        except IndexError:
            continue
    return 0


def run():
    for ind, row in giata_facts_used.iterrows():
        if math.isnan(row["LOB_GIATA"]):
            continue
        giata_id = int(row["LOB_GIATA"])

        if giata_id not in grouped_offers.index:
            print(f"No offers found for {giata_id}.")
            continue

        # series = pd.Series()

        imputed.at[giata_id, 'category_official'] = min_max_scaler(
            mean_imputer(float(row['category_official']), giata_facts_used_desc, 'category_official'),
            giata_facts_used_desc, 'category_official')

        for col_name in ["distance_barspubs", "distance_busstation", "distance_citycentre",
                         "distance_crosscountryskiing",
                         "distance_forest", "distance_golfcourse", "distance_lake", "distance_nightclubs",
                         "distance_park",
                         "distance_publictransport", "distance_restaurants", "distance_river", "distance_sea",
                         "distance_shopping",
                         "distance_skiarea", "distance_skilift", "distance_station", "distance_touristcentre",
                         "distance_trainstation", "location_beach", "location_centrallysituated"]:
            imputed.at[giata_id, col_name] = force_01(zero_imputer(distance_scaler(row[col_name])))

        for col_name in ["location_locatedonmainroad",
                         "location_quietlysituated",
                         "entertainment_childcare", "entertainment_entertainment", "entertainment_entertainmentadults",
                         "entertainment_entertainmentchildren", "entertainment_livemusic", "entertainment_miniclub",
                         "entertainment_minidisco", "facilities_babysitter", "facilities_balcony", "facilities_bar",
                         "facilities_bicyclerental", "facilities_bicyclestorageroom", "facilities_breakfastroom",
                         "facilities_cafe",
                         "facilities_carrental", "facilities_casino", "facilities_childrenspool",
                         "facilities_deckchairs",
                         "facilities_disco", "facilities_dvdrental", "facilities_fireplace", "facilities_garden",
                         "facilities_indoorpool", "facilities_internetaccess", "facilities_library",
                         "facilities_newsstand",
                         "facilities_nightclub", "facilities_outdoorpool", "facilities_parasols",
                         "facilities_playground",
                         "facilities_playroom", "facilities_pool", "facilities_poolbar", "facilities_pub",
                         "facilities_refectory",
                         "facilities_restaurant", "facilities_roomservice", "facilities_safe", "facilities_shops",
                         "facilities_snackbar", "facilities_solarium", "facilities_souvenirshop",
                         "facilities_sunterrace",
                         "facilities_supermarket", "facilities_terrace", "facilities_tourdesk", "facilities_transfer",
                         "facilities_tvroom", "facilities_waterslide", "facilities_wheelchairaccessible",
                         "facilities_wifi", 'facilities_golfdesk',
                         "meals_ai", "meals_alcoholicdrinks", "meals_allinclusivespecial", "meals_bedandbreakfast",
                         "meals_breakfast", "meals_brunch", "meals_childrensmenu", "meals_dietary", "meals_dinner",
                         "meals_drinksincluded", "meals_fullboard", "meals_glutenfree", "meals_halfboard",
                         "meals_internationalbrands", "meals_lunch", "meals_minibar", "meals_picnic",
                         "meals_roomservice",
                         "meals_showcooking", "meals_snacks", "meals_softdrinks", "meals_specialoffers",
                         "meals_vegetarian",
                         "meals_welcomedrink", "misc_carrental", "rooms_aircon", "rooms_balcony", "rooms_bathtub",
                         "meals_ownbrand",
                         "rooms_bidet",
                         "rooms_electricshaver",
                         "rooms_cdplayer", "rooms_childrensbed", "rooms_choicepillow", "rooms_choicetowel",
                         "rooms_cooker",
                         "rooms_cosmetics", "rooms_dishwasher", "rooms_doublebed", "rooms_dvdplayer",
                         "rooms_electriciron",
                         "rooms_familyrooms", "rooms_fridge", "rooms_goodnightservice", "rooms_hairdryer", "rooms_hifi",
                         "rooms_internetaccess", "rooms_kingsizebed", "rooms_kitchen", "rooms_kitchenette",
                         "rooms_lounge",
                         "rooms_microwave", "rooms_minibar", "rooms_minifridge", "rooms_newspaper", "rooms_radio",
                         "rooms_safe",
                         "rooms_seaview", "rooms_sofabed", "rooms_teacoffeemaker", "rooms_terrace",
                         "rooms_trouserpress",
                         "rooms_tv",
                         "rooms_ventilator", "rooms_videogames", "rooms_washingmachine", "rooms_whirlpool",
                         "rooms_wifi",
                         "spa_acupuncture", "spa_antiaging", "spa_ayurveda", "spa_beautyfarm", "spa_beautysalon",
                         "spa_hammam",
                         "spa_healthresort", "spa_hydrotherapy", "spa_massage", "spa_sauna", "spa_spa", "spa_steambath",
                         "spa_thalasso", "spa_treatments", "spa_whirlpool",
                         "sports_curling",
                         "sports_aerobics", "sports_aquaaerobics",
                         "sports_aquafitness", "sports_archery", "sports_badminton", "sports_bananaboat",
                         "sports_basketball",
                         "sports_beachvolleyball", "sports_biking", "sports_billiard", "sports_boccia",
                         "sports_bowling",
                         "sports_canoe", "sports_catamaran", "sports_darts", "sports_fishing", "sports_golf",
                         "sports_gym",
                         "sports_gymnastics", "sports_handball", "sports_horsebackriding", "sports_jetski",
                         "sports_kayak",
                         "sports_kitesurfing", "sports_miniaturegolf", "sports_motorboat", "sports_nordicwalking",
                         "sports_paddletennis", "sports_paragliding", "sports_pedalboat", "sports_puttinggreen",
                         "sports_safari",
                         "sports_sailing", "sports_scubadiving", "sports_skiing", "sports_skipass", "sports_sleighing",
                         "sports_snorkelling", "sports_snowboarding", "sports_squash", "sports_surfing",
                         "sports_tabletennis",
                         "sports_tennis", "sports_volleyball", "sports_waterski", "sports_watersports",
                         "sports_windsurfing",
                         "sports_yoga", "type_adultsonly", "type_airporthotel", "type_apartmenthotel",
                         "type_appartment",
                         "type_beachhotel", "type_boutiquehotel", "type_bungalowcomplex", "type_campground",
                         "type_casinoresort",
                         "type_cityhotel", "type_clubresort", "type_cruiser", "type_ecohotel",
                         "type_familyfriendlyhotel",
                         "type_finca", "type_golfhotel", "type_guesthouse", "type_hikershotel", "type_historicalhotel",
                         "type_hostel", "type_lodge", "type_mountainhotel", "type_mountainlodge", "type_residence",
                         "type_ruralhouse", "type_skihotel", "type_spacomplex", "type_village",
                         "type_bedandbreakfast",
                         "type_cyclistshotel",
                         "type_floatinghotel",
                         "type_selfsupporter",
                         "type_villa"
                         ]:
            imputed.at[giata_id, col_name] = force_01(zero_imputer(row[col_name]))

        for col_name in ["PRD_SW_PKW", "PRD_SW_BUS", "PRD_SW_SCHIFF", "PRD_SW_FLUG", "PRD_SW_RUND", "PRD_SW_FERN",
                         "PRD_SW_SPORT", "PRD_SW_WELL", "PRD_SW_STADT", "PRD_SW_EVENT", "PRD_SW_PARK", "PRD_SW_FAM",
                         "PRD_SW_PARTNER", "PRD_SW_SINGLE", "PRD_SW_FREUND", "PRD_SW_STORNO", "PRD_SW_WASSER",
                         "PRD_SW_BERG", "PRD_SW_SONDER", "PRD_SW_BAHN", "PRD_SW_AKTIV",
                         "kreuz_schiff_reise", "wellness_urlaub", "familien_urlaub", "event_reise", "gruppenreise",
                         "individualreise", "rund_reise"]:
            imputed.at[giata_id, col_name] = force_01(zero_imputer(grouped_offers.at[giata_id, col_name]))

        climate = get_climate2(grouped_offers.at[giata_id, 'Latitude'], grouped_offers.at[giata_id, 'Longitude'])
        imputed.at[giata_id, 'climate_tropic'] = 1 if 1 <= climate <= 3 else 0
        imputed.at[giata_id, 'climate_arid'] = 1 if 4 <= climate <= 7 else 0
        imputed.at[giata_id, 'climate_temperate'] = 1 if 8 <= climate <= 16 else 0
        imputed.at[giata_id, 'climate_cold'] = 1 if 17 <= climate <= 28 else 0
        imputed.at[giata_id, 'climate_polar'] = 1 if 29 <= climate < 30 else 0

        for col_name in ["PRD_BEZ", "ADR_STRASSE", "ADR_PLZ", "GEO_BEZ", "DBU_LNDCODE", 'Latitude', 'Longitude']:
            hotel_information.at[giata_id, col_name] = grouped_offers.at[giata_id, col_name]

    ignored_columns = {
        "LOB_GIATA": "GIATA ID, ignored because it is used as index.",
        "anzahl.x": "Bedeutung unbekannt",
        "anzahl.y": "Bedeutung unbekannt",
        "distance_beach": "Does not contain any values and is subsumed by location_beach.",
        "category_recommended": "Removed because it correlates strongly with category_official,"
                                " therefore does not add any new information.",
        "objectinformation_address_street": "Hotel information, Not used for ML methods. "
                                            " Hotel information we take from offerte_categories_giata, because "
                                            "that file also contains coordinates and the name "
                                            "of the hotel.",
        "objectinformation_address_streetnumber": "Hotel information, Not used for ML methods. "
                                                  " Hotel information we take from offerte_categories_giata, because "
                                                  "that file also contains coordinates and the name "
                                                  "of the hotel.",
        "objectinformation_address_postalcode": "Hotel information, Not used for ML methods. "
                                                " Hotel information we take from offerte_categories_giata, because "
                                                "that file also contains coordinates and the name "
                                                "of the hotel.",
        "objectinformation_address_pobox": "Hotel information, Not used for ML methods. "
                                           " Hotel information we take from offerte_categories_giata, because "
                                           "that file also contains coordinates and the name "
                                           "of the hotel.",
        "objectinformation_address_cityname": "Hotel information, Not used for ML methods. "
                                              " Hotel information we take from offerte_categories_giata, because "
                                              "that file also contains coordinates and the name "
                                              "of the hotel.",
        "objectinformation_address_country": "Hotel information, Not used for ML methods. "
                                             " Hotel information we take from offerte_categories_giata, because "
                                             "that file also contains coordinates and the name "
                                             "of the hotel.",
        "objectinformation_address_addresslines": "Hotel information, Not used for ML methods. "
                                                  " Hotel information we take from offerte_categories_giata, because "
                                                  "that file also contains coordinates and the name "
                                                  "of the hotel.",
        "objectinformation_chain": "Hotel information, Not used for ML methods. "
                                   " Hotel information we take from offerte_categories_giata, because "
                                   "that file also contains coordinates and the name "
                                   "of the hotel.",
        "objectinformation_email": "Hotel information, Not used for ML methods. "
                                   " Hotel information we take from offerte_categories_giata, because "
                                   "that file also contains coordinates and the name "
                                   "of the hotel.",
        "objectinformation_fax": "Hotel information, Not used for ML methods. "
                                 " Hotel information we take from offerte_categories_giata, because "
                                 "that file also contains coordinates and the name "
                                 "of the hotel.",
        "objectinformation_phone": "Hotel information, Not used for ML methods. "
                                   " Hotel information we take from offerte_categories_giata, because "
                                   "that file also contains coordinates and the name "
                                   "of the hotel.",
        "objectinformation_url": "Hotel information, Not used for ML methods. "
                                 " Hotel information we take from offerte_categories_giata, because "
                                 "that file also contains coordinates and the name "
                                 "of the hotel.",
        "DBU_SVNO": "ID. Not relevant for the result.",
        "DBU_PRDCODE": "ID. Not relevant for the result.",
        "DBU_LOBCODE": "ID. Not relevant for the result.",
        "DBU_GIATA_ID": "Ignored because it is used as an index.",
        "DBU_LNDCODE": "Hotel information, not relevant for machine learning, but is used to identify the hotel.",
        "PRD_BEZ": "Hotel information, not relevant for machine learning, but is used to identify the hotel.",
        "ADR_STRASSE": "Hotel information, not relevant for machine learning, but is used to identify the hotel.",
        "ADR_PLZ": "Hotel information, not relevant for machine learning, but is used to identify the hotel.",
        "GEO_BEZ": "Hotel information, not relevant for machine learning, but is used to identify the hotel.",
        "anzahl": "Probably not relevant, maybe number of bookings?",
        "dauer": "Duration of the product. Not relevant for machine learning.",
        "reklamation": "Maybe number or probability of reclamations. Not relevant for ML.",
        "ADR_GEOCODE": "City name. Not relevant for machine learning.",
        "ADR_MAP_X": "Translated to climate data. And used for map drawing.",
        "ADR_MAP_Y": "Translated to climate data. And used for map drawing.",
        "ADR_MAP_QUALITY": "Not relevant for ML.",
        "VERSIONID": "Not relevant for ML.",
        "SEQ_PRD_CODE": "Not relevant for ML.",
        "bezeichnung": "Not relevant for ML.",
        "kategorie": "Removed because it correlates strongly with category_official,"
                     " therefore does not add any new information.",
        "country": "Not relevant for machine learning.",
        "accomodation": "Not relevant for machine learning.",
        "vertrieb": "Not relevant for machine learning.",
        "svno_fehler_2": "Meaning unknown, probably not relevant for machine learning.",
        "indikator_giata": "Meaning unknown, removed because it is not interpretable.",
        "indikator_lob_giata": "Meaning unknown, removed because it is not interpretable.",
        "indikator_giata_final": "Meaning unknown, removed because it is not interpretable.",
        "giata_unbekannt": "Meaning unknown, removed because it is not interpretable.",
        "traveltype": "Already coded in other columns (PRD_SW_PKW"
                      "	PRD_SW_BUS	PRD_SW_SCHIFF	PRD_SW_FLUG	PRD_SW_RUND usw)",
        "Berge": "Derived category by Grossmann (2008)",
        "Meer_Kueste_Strand": "Derived category by Grossmann (2008)",
        "Seen": "Derived category by Grossmann (2008)",
        "Stadt": "Derived category by Grossmann (2008)",
        "Natur_Landschaft": "Derived category by Grossmann (2008)",
        "warm": "Derived category by Grossmann (2008)",
        "kalt": "Derived category by Grossmann (2008)",
        "sonnig_heiß": "Derived category by Grossmann (2008)",
        "mild": "Derived category by Grossmann (2008)",
        "kuehl": "Derived category by Grossmann (2008)",
        "Wellness_Spas": "Derived category by Grossmann (2008)",
        "Kunst._Kultur": "Derived category by Grossmann (2008)",
        "Sehenswuerdigkeiten": "Derived category by Grossmann (2008)",
        "Gastronomie": "Derived category by Grossmann (2008)",
        "Nightlife": "Derived category by Grossmann (2008)",
        "History": "Derived category by Grossmann (2008)",
        "Touren": "Derived category by Grossmann (2008)",
        "Maerkte": "Derived category by Grossmann (2008)",
        "Unterhaltungsevents": "Derived category by Grossmann (2008)",
        "Wintersport": "Derived category by Grossmann (2008)",
        "Sommersport": "Derived category by Grossmann (2008)",
        "Extremsport": "Derived category by Grossmann (2008)",
        "Bewegung_und_Sport": "Derived category by Grossmann (2008)",
        "Essen": "Derived category by Grossmann (2008)",
        "Trinken": "Derived category by Grossmann (2008)",
        "Sightseeing": "Derived category by Grossmann (2008)",
        "Shopping": "Derived category by Grossmann (2008)",
        "Entertainment": "Derived category by Grossmann (2008)",
        "Spazieren_Wanderm": "Derived category by Grossmann (2008)",
        "Erholungsaktiviäten": "Derived category by Grossmann (2008)",
        "Natur_beobachten": "Derived category by Grossmann (2008)",
        "Kurse_Workshops": "Derived category by Grossmann (2008)",
        "Kulturaktivitaeten": "Derived category by Grossmann (2008)",
        "relaxing_recreative": "Derived category by Grossmann (2008)",
        "family_friendly": "Derived category by Grossmann (2008)",
        "exciting_thrilling": "Derived category by Grossmann (2008)",
        "entertaining": "Derived category by Grossmann (2008)",
        "calming": "Derived category by Grossmann (2008)",
        "exclusive_gehoben": "Derived category by Grossmann (2008)",
        "alternativ_guenstig": "Derived category by Grossmann (2008)",
        "Adventure": "Derived category by Grossmann (2008)",
        "Romantik": "Derived category by Grossmann (2008)",
        "PRD_SW_E2C_SPLIT": "Not interpretable.",
        "PRD_SW_EK": "Not interpretable."
    }

    for col in giata_facts_used:
        if col not in imputed and col not in ignored_columns:
            print(f"        \"{col}\": \"\",")
    for col in offerte_categories_giata:
        if col not in imputed and col not in ignored_columns:
            print(f"        \"{col}\": \"\",")

    imputed_reindexed = imputed.reindex(sorted(imputed.columns, key=str.lower), axis=1)

    imputed_reindexed.sort_index(inplace=True)

    os.makedirs("output", exist_ok=True)
    imputed_reindexed.to_csv("output/imputed.csv")
    imputed_reindexed.replace(0, np.nan).describe().to_csv("output/imputed_describe.csv")
    hotel_information.to_csv("output/hotel-information.csv")


run()
