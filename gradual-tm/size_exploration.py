import random
from typing import Union

import gensim.corpora
import pandas as pd
from gensim.models import LdaModel, LsiModel, CoherenceModel
from gensim.models.nmf import Nmf
import numpy as np

import load_corpus

multiplier: int = 10
prefix: str = "gradual"

# Print head
processed_data, id2word, corpus_data = load_corpus.load_corpus(multiplier)



def build(start: int, stop: int, output_suffix=""):
    population_file = f'output/gradual-tm-population-LsiModel-{output_suffix}.csv'

    try:
        population = pd.read_csv(population_file, index_col='index')
    except FileNotFoundError:
        population = pd.DataFrame()
        population.index.name = 'index'

    for size in range(start, stop + 1):
        for retry in range(10):
            index = f"{size}_{retry}"

            if index in population.index:
                continue

            print(f"Exploring {index}")

            individual: pd.Series = pd.Series(name=index, dtype=np.float64)

            decay = 0.604495123356499
            extra_samples = 1.29831850748134
            power_iters = 0.348351861512661

            topic_model = LsiModel(corpus=corpus_data,
                                   num_topics=size,
                                   id2word=id2word,
                                   decay=decay,
                                   power_iters=int(round(power_iters)),
                                   extra_samples=int(round(extra_samples)))

            coherence_model = CoherenceModel(model=topic_model, texts=processed_data, dictionary=id2word,
                                             coherence='c_v')
            individual["size"] = size
            individual["coherence"] = coherence_model.get_coherence()

            if population.empty or population['coherence'].max() < individual['coherence']:
                topic_model.save(f"output/gradual-tm-LsiModel-{output_suffix}")

            population = population.append(individual)
            population.sort_index(inplace=True)
            population.to_csv(population_file)

            print(population.sort_values('coherence', ascending=False).head())


build(22, 29, "22-29")
build(15, 21, "15-21")
build(7, 14, "7-14")
