import types

import gensim
import pandas as pd


def load_corpus(multiplier):
    imputed_data = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')

    processed_data = [[w for w, c in row.iteritems() for _ in range(round(c * multiplier))] for ind, row in
                      imputed_data.iterrows()]

    id2word = gensim.corpora.Dictionary(processed_data)
    corpus_data = [id2word.doc2bow(line) for line in processed_data]

    return processed_data, id2word, corpus_data
