import math
import os
import random
import sys
from typing import Union, List

import gensim.corpora
import pandas as pd
from gensim.models import LdaModel, CoherenceModel, LsiModel
from gensim.models.lsimodel import P2_EXTRA_ITERS, P2_EXTRA_DIMS
from gensim.models.nmf import Nmf

import load_corpus


multiplier: int = 1
prefix: str = "binary"
# multiplier: int = 10
# prefix: str = "gradual"

# Print head
processed_data, id2word, corpus_data = load_corpus.load_corpus(multiplier)


def generate(name: int, search_space: List[str]) -> pd.Series:
    individual: pd.Series = pd.Series(name=name)

    if "num_topics" in search_space:
        individual["num_topics"] = random.uniform(37 * 0.8, 37 * 1.2)
    if "passes" in search_space:
        individual["passes"] = random.uniform(1, 20)
    if "eta" in search_space:
        individual["eta"] = random.random()
    if "iterations" in search_space:
        individual["iterations"] = random.uniform(1, 100)
    if "decay" in search_space:
        individual["decay"] = random.uniform(0.5, 1)
    if "power_iters" in search_space:
        individual["power_iters"] = random.uniform(0, P2_EXTRA_ITERS * 2)
    if "extra_samples" in search_space:
        individual["extra_samples"] = random.uniform(0, P2_EXTRA_DIMS * 2)
    if "kappa" in search_space:
        individual["kappa"] = random.random()
    individual["mutation_rate"] = 0.2

    return individual


def mutate(individual: pd.Series, new_name: int, search_space: List[str]) -> pd.Series:
    tau = 1 / math.sqrt(len(search_space))

    individual = individual.drop('coherence')
    individual["parent"] = individual.name
    individual.name = new_name

    individual["mutation_rate"] *= math.exp(tau * random.normalvariate(0, 1))

    for ind in search_space:
        individual[ind] *= random.normalvariate(1, individual["mutation_rate"])

    return individual


def train(individual: pd.Series, model: str) -> Union[LdaModel, LsiModel, Nmf, None]:
    if model == "LdaModel":
        if not (37 * 0.8 < individual["num_topics"] < 37 * 1.2):
            return None
        if not (1 <= int(round(individual["passes"]))):
            return None
        if not (0 <= individual["eta"] <= 1):
            return None
        if not (1 <= int(round(individual["iterations"]))):
            return None
        if not (0.5 < individual["decay"] <= 1):
            return None

        return LdaModel(corpus=corpus_data,
                        id2word=id2word,
                        num_topics=int(round(individual["num_topics"])),
                        alpha='auto',
                        passes=int(round(individual["passes"])),
                        eta=individual["eta"],
                        iterations=int(round(individual["iterations"])),
                        decay=individual["decay"])

    if model == "LsiModel":
        if not (37 * 0.8 < individual["num_topics"] < 37 * 1.2):
            return None
        if not (0.5 < individual["decay"] <= 1):
            return None
        if not (0 <= int(round(individual["power_iters"]))):
            return None
        if not (0 <= int(round(individual["extra_samples"]))):
            return None

        return LsiModel(corpus=corpus_data,
                        num_topics=int(round(individual["num_topics"])),
                        id2word=id2word,
                        decay=individual["decay"],
                        power_iters=int(round(individual["power_iters"])),
                        extra_samples=int(round(individual["extra_samples"])))

    if model == "Nmf":
        if not (37 * 0.8 < individual["num_topics"] < 37 * 1.2):
            return None
        if not (1 <= int(round(individual["passes"]))):
            return None
        if not (0 < individual["kappa"] <= 1):
            return None

        return Nmf(corpus=corpus_data,
                   num_topics=int(round(individual["num_topics"])),
                   id2word=id2word,
                   passes=int(round(individual["passes"])),
                   kappa=individual["kappa"])


def run(population: pd.DataFrame, new_index: int, search_space: List[str], population_folder: str, population_file: str,
        model: str) -> Union[pd.DataFrame, None]:
    if new_index in population.index:
        return population

    if new_index < len(search_space) * 100:
        individual: pd.Series = generate(new_index, search_space)
    else:
        # Fallback setting. If the following method fails for any reason, we already have an individual.
        individual: pd.Series = population.sample().iloc[0]

        parents = population.sort_values('coherence', ascending=False).head(len(search_space) * 100)

        rand = random.uniform(0, parents['coherence'].sum())
        running_total = 0
        for ind, candidate in parents.iterrows():
            # For NaN candidates
            if not (candidate['coherence'] > 0):
                continue

            running_total += candidate['coherence']
            individual = candidate
            if running_total >= rand:
                break

        individual: pd.Series = mutate(individual, new_index, search_space)

    print("Training:")
    print(individual)
    topic_model: Union[LdaModel, LsiModel, Nmf] = train(individual, model)

    if topic_model is None:
        print("Unviable individual!")
        return None

    coherence_model = CoherenceModel(model=topic_model, texts=processed_data, dictionary=id2word, coherence='c_v')
    individual["coherence"] = coherence_model.get_coherence()
    print("Coherence ", individual["coherence"])

    topic_model.save(f"{population_folder}/{new_index}")

    if population.empty or population['coherence'].max() < individual['coherence']:
        topic_model.save(f"output/{prefix}-tm-{model}")

    population = population.append(individual)
    population.sort_index(inplace=True)
    population.to_csv(population_file)

    print(population.sort_values('coherence', ascending=False).head())

    return population


def train_models(model: str):
    if model == "LdaModel":
        search_space = ["num_topics", "passes", "eta", "iterations", "decay"]
    elif model == "LsiModel":
        search_space = ["num_topics", "decay", "power_iters", "extra_samples"]
    elif model == "Nmf":
        search_space = ["num_topics", "passes", "kappa"]
    else:
        raise Exception()

    population_file = f"output/{prefix}-tm-population-{model}.csv"
    population_folder = f"/home/ircecho/{prefix}-tm-exploration-{model}/"

    os.makedirs(population_folder, exist_ok=True)

    if os.path.isfile(population_file):
        pop: pd.DataFrame = pd.read_csv(population_file, index_col='index')
    else:
        pop: pd.DataFrame = pd.DataFrame(index=pd.Series(name="index"))

    for i in range(len(search_space) * 2000):
        new_pop = None
        while new_pop is None:
            new_pop = run(pop, i, search_space, population_folder, population_file, model)
        pop = new_pop


models = ["LdaModel", "Nmf", "LsiModel"]

model = sys.argv[1]

if model not in models:
    raise Exception("First command line argument must be one of " + str(models))

train_models(model)
