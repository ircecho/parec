from typing import Union, Type

import gensim.corpora
import pandas as pd
from gensim.models import LsiModel, LdaModel
from gensim.models.nmf import Nmf

import load_corpus

imputed_data = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')


def build(model_type: Type, file_name: str, multiplier: int):
    model: Union[LdaModel, LsiModel, Nmf]
    model = model_type.load(f'output/{file_name}')

    with open(f'output/{file_name}-description.txt', 'w') as f:
        for ind, top in model.show_topics(100000, 100000):
            print(ind, top, file=f)
    with open(f'output/{file_name}-hr-description.txt', 'w') as f:
        for ind, top in model.show_topics(100000, 7):
            print(ind, top, file=f)

    processed_data, id2word, corpus_data = load_corpus.load_corpus(multiplier)

    data = []
    for h in corpus_data:
        r = dict(model[h])
        data.append([r.get(i, 0) for i in range(0, model.num_topics)])
    rep = pd.DataFrame(data, imputed_data.index)
    rep.to_csv(f"../clustering/representations/{file_name}.csv")


build(LsiModel, "binary-tm-LsiModel", 1)
build(LdaModel, "binary-tm-LdaModel", 1)
build(Nmf, "binary-tm-Nmf", 1)
build(LsiModel, "gradual-tm-LsiModel", 10)
build(LdaModel, "gradual-tm-LdaModel", 10)
build(Nmf, "gradual-tm-Nmf", 10)
build(LsiModel, "gradual-tm-LsiModel-7-14", 10)
build(LsiModel, "gradual-tm-LsiModel-15-21", 10)
build(LsiModel, "gradual-tm-LsiModel-22-29", 10)
