import pandas as pd

binary_data = pd.read_csv('../data-preprocessing/output/imputed.csv', index_col='LOB_GIATA')


def convert(clustering_name: str, directory: str = ""):
    result = pd.DataFrame(index=binary_data.index)
    clustering = pd.read_csv(f"output/{clustering_name}", header=None)

    if not sorted(binary_data.columns) == sorted(clustering[0]):
        raise Exception("Clustered columns do not equate data.")

    for z in range(0, clustering[1].max() + 1):
        cats = clustering[clustering[1] == z][0].tolist()

        result_column = pd.Series(0, index=binary_data.index)

        for cat in cats:
            result_column += binary_data[cat]

        result_column /= len(cats)
        result[z] = result_column

    result.to_csv(
        f"../clustering/representations/{directory}/wv-{clustering_name.replace('best_clustering_', '').replace('_', '-')}")


convert('best_clustering_no_cat.csv')
convert('best_clustering_no_cat_no_freq.csv')
convert('best_clustering_no_cat_own_freq.csv')
convert('best_clustering_with_cat.csv')
convert('best_clustering_with_cat_no_freq.csv')
convert('best_clustering_with_cat_own_freq.csv')

convert('best_clustering_no_cat_own_freq-7-14.csv', 'wv-size-exploration')
convert('best_clustering_no_cat_own_freq-15-21.csv', 'wv-size-exploration')
convert('best_clustering_no_cat_own_freq-22-29.csv', 'wv-size-exploration')
