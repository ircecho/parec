import random

import pandas as pd
import sklearn

# input
from spherecluster.spherecluster import SphericalKMeans


def cluster(suffix: str, start: int, stop: int, output_suffix=""):
    frame_normalized = pd.read_csv("output/sentence_embeddings_normalized" + suffix + ".csv", header=None, index_col=0)

    best_silhouette_score = 0

    for n_clusters in range(start, stop + 1):
        for i in range(1000):
            k_means = SphericalKMeans(n_clusters=n_clusters, init='random')
            model = k_means.fit(frame_normalized)

            silhouette_score = sklearn.metrics.silhouette_score(frame_normalized, model.labels_, metric='cosine')

            print(i, n_clusters, silhouette_score, sep=',')

            f = open("output/parameters" + suffix + output_suffix + ".csv", "a")

            print(i, n_clusters, silhouette_score, file=f, sep=',')

            f.close()

            if silhouette_score <= best_silhouette_score:
                continue

            best_silhouette_score = silhouette_score
            print("New record.")

            clustering = pd.DataFrame(model.labels_, index=frame_normalized.index)
            clustering.to_csv("output/best_clustering" + suffix + output_suffix + ".csv", header=False)

            f = open("output/best_clustering_hr" + suffix + output_suffix + ".txt", "w")

            for z in range(0, clustering[0].max() + 1):
                cats = clustering[clustering[0] == z].index.values
                f.write(' '.join(str(x) for x in cats) + "\n")

            f.close()

            f = open("output/best_parameters" + suffix + output_suffix + ".txt", "w")

            print("n_clusters = " + str(n_clusters), file=f)
            print("silhouette_score = " + str(silhouette_score), file=f)

            f.close()


# cluster("_no_cat", round(37 * 0.8), round(37 * 1.2))
# cluster("_with_cat", round(37 * 0.8), round(37 * 1.2))
# cluster("_no_cat_own_freq", round(37 * 0.8), round(37 * 1.2))
# cluster("_with_cat_own_freq", round(37 * 0.8), round(37 * 1.2))
# cluster("_no_cat_no_freq", round(37 * 0.8), round(37 * 1.2))
# cluster("_with_cat_no_freq", round(37 * 0.8), round(37 * 1.2))

for band in [(7, 14), (15, 21), (22, 29)]:
    cluster("_no_cat_own_freq", band[0], band[1], f"-{band[0]}-{band[1]}")
