import multiprocessing as mp
import time

import pandas as pd
import sklearn.preprocessing

# input
from spherecluster.spherecluster import SphericalKMeans

frame_normalized = pd.read_csv("output/sentence_embeddings_normalized_no_cat_own_freq.csv", header=None,
                               index_col=0)

clustering = pd.read_csv("output/best_clustering_no_cat_own_freq.csv", header=None)

clustering_array = [[] for i in range(clustering[1].max() + 1)]

for ind, row in clustering.iterrows():
    clustering_array[row[1]].append(row[0])

clustering_array = list(sorted(clustering_array, key=lambda l: -len(l)))

attribute_names = {
    "rooms_bathtub": "Attribute 1",
    "PRD_SW_SPORT": "Attribute 2",
    "facilities_waterslide": "Attribute 3",
    "entertainment_childcare": "Attribute 4",
    "familien_urlaub": "Attribute 5",
    "distance_crosscountryskiing": "Attribute 6",
    "meals_alcoholicdrinks": "Attribute 7",
    "category_official": "Attribute 8",
    "facilities_deckchairs": "Attribute 9",
    "distance_citycentre": "Attribute 10",
    "distance_busstation": "Attribute 11",
    "facilities_dvdrental": "Attribute 12",
    "PRD_SW_FREUND": "Attribute 13",
    "distance_barspubs": "Attribute 14",
    "spa_antiaging": "Attribute 15",
    "distance_forest": "Attribute 16",
    "distance_nightclubs": "Attribute 17",
    "facilities_balcony": "Attribute 18",
    "distance_restaurants": "Attribute 19",
    "distance_golfcourse": "Attribute 20",
    "individualreise": "Attribute 21",
    "facilities_internetaccess": "Attribute 22",
    "facilities_breakfastroom": "Attribute 23",
    "facilities_bicyclerental": "Attribute 24",
    "entertainment_entertainment": "Attribute 25",
    "kreuz_schiff_reise": "Attribute 26",
    "distance_shopping": "Attribute 27",
    "meals_fullboard": "Attribute 28",
    "sports_aerobics": "Attribute 29",
    "distance_lake": "Attribute 30",
    "sports_kitesurfing": "Attribute 31",
    "spa_acupuncture": "Attribute 32",
    "facilities_safe": "Attribute 33",
    "PRD_SW_WELL": "Attribute 34",
    "distance_park": "Attribute 35",
    "facilities_roomservice": "Attribute 36",
    "facilities_parasols": "Attribute 37",
    "facilities_library": "Attribute 38",
    "facilities_souvenirshop": "Attribute 39",
    "climate_arid": "Attribute 40",
    "facilities_casino": "Attribute 41",
    "rooms_aircon": "Attribute 42",
    "sports_fishing": "Attribute 43",
    "climate_cold": "Attribute 44"
}
i = 1

centroids = pd.DataFrame(index=list(attribute_names.values()), columns=frame_normalized.columns)

for attribute in clustering_array:
    i = i + 1
    non_norm_vector = frame_normalized[frame_normalized.index.isin(attribute)].sum() / len(attribute)

    idx = attribute_names[attribute[0]]

    centroids.loc[idx] = non_norm_vector

centroids_normalized = pd.DataFrame(columns=centroids.columns, index=centroids.index,
                                    data=sklearn.preprocessing.normalize(centroids, norm="l2", axis=1, copy=True,
                                                                         return_norm=False))

centroids_normalized.to_csv("output/centroids-normalized.csv")

def research_clustering(n_clusters: int):
    best_silhouette_score = float('nan')
    best_labels = None

    k_means = SphericalKMeans(n_clusters=n_clusters, init='random')

    for _ in range(100_000):
        model = k_means.fit(centroids_normalized)
        silhouette_score = sklearn.metrics.silhouette_score(centroids_normalized, model.labels_, metric='cosine')

        if silhouette_score <= best_silhouette_score:
            continue

        best_silhouette_score = silhouette_score
        best_labels = model.labels_

    print("Clusters:", n_clusters, "Score:", best_silhouette_score)

    cl = pd.DataFrame(best_labels, index=centroids_normalized.index)
    f = open(f"output/best_group_grouping_{n_clusters}.txt", "w")

    print("n_clusters = " + str(n_clusters), file=f)
    print("silhouette_score = " + str(best_silhouette_score), file=f)

    for z in range(0, cl[0].max() + 1):
        cats = cl[cl[0] == z].index.values
        print('; '.join(str(x) for x in cats), file=f)

    f.close()


start = time.time()

# Step 1: Init multiprocessing.Pool()
pool = mp.Pool(mp.cpu_count() - 1)

pool.map(research_clustering, range(2, 7 + 6 + 1))

pool.close()

end = time.time()
print("Time elapsed: ", end - start)
