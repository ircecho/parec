import pandas as pd
import sklearn
from scipy import spatial
from sklearn.manifold import TSNE

frame_normalized = pd.read_csv("output/sentence_embeddings_no_cat_own_freq.csv", header=None, index_col=0)

tsne_wp_3d = sklearn.decomposition.PCA(n_components=2)
embeddings_wp_3d = tsne_wp_3d.fit_transform(frame_normalized)

embedding_normalized = sklearn.preprocessing.normalize(embeddings_wp_3d, norm="l2", axis=1, copy=True, return_norm=False)
resdf = pd.DataFrame(data=embedding_normalized, index=frame_normalized.index)

for c in resdf.index:
    if "golf" in str(c) or c == 'facilities_wifi':
        s = resdf.loc[c]
        latext = str(c).replace('_', '\\_')
        print(f"\\draw[->,Mahogany,thick] (O) -- ({s[0]},{s[1]}) node[black,fill=white,rounded corners=2pt,inner sep=1pt,outer sep=2pt,anchor=south]{{\\scriptsize {latext} = [{s[0]:.2f} {s[1]:.2f}]}};")
