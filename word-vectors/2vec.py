import csv
import os
from collections import Counter
from typing import List

import pandas as pd
import sklearn

import SIF_embedding
import data_io
import params

# input

weightpara = 1e-3  # the parameter in the SIF weighting scheme, usually in the range [3e-5, 3e-3]
rmpc = 1  # number of principal components to remove in SIF weighting scheme
index = []
sent_with_cat = []
sent_no_cat = []

words, We = data_io.getWordmap('input/wordembeddings.txt')


def getWordWeight(corpus: List[str], a=1e-3):
    if a <= 0:  # when the parameter makes no sense, use unweighted
        a = 1.0

    freq = Counter(corpus)
    word2weight = {}

    for key in words:
        word2weight[key] = a / (a + freq[key] / len(corpus))
    return word2weight


with open('input/column_names.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        index.append(row[0])
        list_with_cat = list(filter(lambda p: p != '', row[1:]))
        list_no_cat = list(filter(lambda p: p != '', row[2:]))
        sent_with_cat.append(list_with_cat)
        sent_no_cat.append(list_no_cat)


def calculate_save(sentences, word2weight, suffix: str):
    # load word vectors
    x, m = data_io.sentences2idx(sentences, words)
    # load word weights
    weight4ind = data_io.getWeight(words, word2weight)  # weight4ind[i] is the weight for the i-th word
    # load sentences
    w = data_io.seq2weight(x, m, weight4ind)  # get word weights
    # set parameters
    par = params.params()
    par.rmpc = rmpc
    # get SIF embedding
    embedding = SIF_embedding.SIF_embedding(We, x, w, par)  # embedding[i,:] is the embedding for sentence i
    embedding_normalized = sklearn.preprocessing.normalize(embedding, norm="l2", axis=1, copy=True, return_norm=False)

    os.makedirs("output", exist_ok=True)

    frame: pd.DataFrame = pd.DataFrame(embedding, index=index)
    frame.to_csv("output/sentence_embeddings" + suffix + ".csv", header=False)
    frame_normalized: pd.DataFrame = pd.DataFrame(embedding_normalized, index=index)
    frame_normalized.to_csv("output/sentence_embeddings_normalized" + suffix + ".csv", header=False)


all_english_frequencies = data_io.getWordWeight("input/frequencies.txt", weightpara)
no_cat_frequencies = getWordWeight([item for sublist in sent_no_cat for item in sublist], weightpara)
with_cat_frequencies = getWordWeight([item for sublist in sent_with_cat for item in sublist], weightpara)
no_frequencies = getWordWeight(list(set([item for sublist in sent_with_cat for item in sublist])), weightpara)


calculate_save(sentences=sent_no_cat, word2weight=all_english_frequencies, suffix="_no_cat")
calculate_save(sentences=sent_with_cat, word2weight=all_english_frequencies, suffix="_with_cat")
calculate_save(sentences=sent_no_cat, word2weight=no_cat_frequencies, suffix="_no_cat_own_freq")
calculate_save(sentences=sent_with_cat, word2weight=with_cat_frequencies, suffix="_with_cat_own_freq")
calculate_save(sentences=sent_no_cat, word2weight=no_frequencies, suffix="_no_cat_no_freq")
calculate_save(sentences=sent_with_cat, word2weight=no_frequencies, suffix="_with_cat_no_freq")
