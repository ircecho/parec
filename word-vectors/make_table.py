import pandas as pd

clustering = pd.read_csv("output/best_clustering_no_cat_own_freq.csv", header=None)

clustering_array = [[] for i in range(clustering[1].max() + 1)]

for ind, row in clustering.iterrows():
    clustering_array[row[1]].append(row[0])

clustering_array = list(sorted(clustering_array, key=lambda l: -len(l)))

enumerator = 1
for p_ind, part in enumerate(
        [clustering_array[:len(clustering_array) // 2], clustering_array[len(clustering_array) // 2:]]):
    print("\\begin{figure}")
    print("\t\\begin{tiny}")
    print("\t\t\\begin{description}")
    print("\t\t\t\\setlength\\itemsep{0.5ex}")
    for ind, arr in enumerate(part):
        attributes = ", ".join(map(lambda att: f"\\att{{{att}}}", arr))
        print("\t\t\t\\item[Attribute ", enumerator, "] ", attributes, sep='')
        enumerator += 1
    print("\t\t\\end{description}")
    print("\t\\end{tiny}")
    print("\t\\caption{Listing of the word embedding derived representation, part " + str(p_ind + 1) + ".")
    print("\t\tAttributes are sorted by size.}")
    print("\t\\label{fig:word-embedding-rep-" + str(p_ind + 1) + "}")
    print("\\end{figure}")
