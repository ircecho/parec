import os

import matplotlib.pyplot as plt
import pandas as pd

silhouette_score = pd.DataFrame(index=range(2, 13 + 1), columns=["SS"])

silhouette_score.loc[3]= 0.11954646179230209
silhouette_score.loc[4]= 0.11541362780056605
silhouette_score.loc[2]= 0.12937863237497219
silhouette_score.loc[6]= 0.1399908018140413
silhouette_score.loc[5]= 0.13565358653510853
silhouette_score.loc[7]= 0.14909102060468682
silhouette_score.loc[8]= 0.16007333879958513
silhouette_score.loc[9]= 0.1620305178754592
silhouette_score.loc[10]= 0.17006351357857058
silhouette_score.loc[11]= 0.1724459329948736
silhouette_score.loc[12]= 0.17585166065542124
silhouette_score.loc[13]= 0.17768081380510004

plt.plot(silhouette_score)

plt.xlabel('Number of clusters')
plt.ylabel('Average silhouette score')


def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png")


save_plot('output/we_group_groupings_plot')
