import os

import pandas as pd
import sklearn
from sklearn.cluster import KMeans

imputed = pd.read_csv("../data-preprocessing/output/imputed.csv", index_col='LOB_GIATA')


def load_representations():
    li = [('original', imputed)]

    for f in sorted(os.listdir("representations")):
        t = os.path.splitext(f)[0]
        representation = pd.read_csv("representations/" + f, index_col='LOB_GIATA')
        li.append((t, representation))

    return li


files = load_representations()

results = []
for tp, rep in files:

    result = []

    for n_clusters in range(2, 49):
        print(str(n_clusters) + " for " + tp)
        scores = []
        for i in range(0, 10):
            clusterer = KMeans(n_clusters=n_clusters)
            preds = clusterer.fit_predict(rep)
            centers = clusterer.cluster_centers_

            score = sklearn.metrics.silhouette_score(rep, preds)
            scores.append(score)
        result.append(max(scores))

    results.append(result)

frame = pd.DataFrame(results, index=map(lambda x: x[0], files), columns=range(2, 49))
frame.index.name = "no_clusters"

os.makedirs("output", exist_ok=True)
frame.to_csv("output/silhouette_score.csv")
