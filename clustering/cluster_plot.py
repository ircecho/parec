import colorsys
import os
import random

import matplotlib.pyplot as plt
import pandas as pd

DIST_PATH = f"output/new_cluster/base_clustering_distance_adj_mi.csv"
dist = pd.read_csv(DIST_PATH, index_col='tp')
dist.rename(columns=int, inplace=True)

# gca stands for 'get current axis'

f = plt.figure()

cols = dist.index

color_dict = {}

tab20 = plt.get_cmap("viridis")

def hsv_add(name: str, id):
    t = colorsys.hsv_to_rgb(id/13, 1, 0.8)
    c = "#{:02x}{:02x}{:02x}".format(round(t[0] * 255), round(t[1] * 255), round(t[2] * 255))
    color_dict[name] = c



hsv_add("AE", 0)

hsv_add("WE NC OF", 1)
hsv_add("WE WC OF", 2)
hsv_add("WE WC NF", 3)
hsv_add("WE NC NF", 4)
hsv_add("WE WC CF", 5)
hsv_add("WE NC CF", 6)

hsv_add("TM G LSI", 7)
hsv_add("TM B LSI", 8)
hsv_add("TM B LDA", 9)
hsv_add("TM G LDA", 10)
hsv_add("TM B NMF", 11)
hsv_add("TM G NMF", 12)

colors = [color_dict.get(x, '#333333') for x in dist.index]
random.shuffle(colors)
markers = (['H', '^', 'v',"<",">", 's', '.', '1', '2', '3', '4', "+", "x"])
random.shuffle(markers)
ax = dist.T.plot(ax=f.gca(), figsize=(16, 8), color=colors)
ax.set_xticks(range(2, 21 + 1))
for i, line in enumerate(ax.get_lines()):
        line.set_marker(markers[i])
plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
#f.subplots_adjust(right=0.8)

plt.xlabel("Number of clusters")
plt.ylabel("AMI")


# plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
#
# dist.T.plot()
# plt.xlabel('Number of clusters')
# plt.ylabel('Average silhouette score')


def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png")


save_plot('output/cluster_plot')

medians = dist.median(axis=1)
mse = (dist.subtract(dist.median(axis=1), axis=0) ** 2).mean()
print(mse)
print(f"Minimum: {mse.idxmin()} clusters.")
