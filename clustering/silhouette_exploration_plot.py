import os

import matplotlib.pyplot as plt
import pandas as pd

silhouette_score = pd.read_csv("output/silhouette_score.csv",index_col='no_clusters')

# gca stands for 'get current axis'
ax = plt.gca()

silhouette_score.T.plot()

plt.xlabel('Number of clusters')
plt.ylabel('Average silhouette score')

def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png")

save_plot('output/silhouette_score_plot')