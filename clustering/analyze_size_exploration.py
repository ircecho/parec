import os

import matplotlib.pyplot as plt
import pandas as pd

wv_data_array = [[7, 0.113130423883515],
                 [8, 0.120883318374182],
                 [9, 0.125220992841702],
                 [10, 0.130593178303302],
                 [11, 0.135104962758708],
                 [12, 0.141945667227855],
                 [13, 0.139810428510911],
                 [14, 0.140892442584287],
                 [15, 0.143022757326821],
                 [16, 0.144492061572147],
                 [17, 0.145578691509276],
                 [18, 0.145338903072668],
                 [19, 0.150297924700832],
                 [20, 0.15268019279198],
                 [21, 0.15361469152519],
                 [22, 0.150324619970019],
                 [23, 0.152814067257519],
                 [24, 0.15597030127304],
                 [25, 0.15622577584408],
                 [26, 0.154779739070042],
                 [27, 0.15887704116624],
                 [28, 0.163472322241908],
                 [29, 0.160008326064659]]

wv_data = pd.DataFrame(wv_data_array, columns=['Number of clusters', 'Mean Silhouette Coefficient']).set_index(
    'Number of clusters')

ae_data_array = [[7, 0.054641712672121],
                 [8, 0.050217018699639],
                 [9, 0.046436949236549],
                 [10, 0.046253924343453],
                 [11, 0.040587627575716],
                 [12, 0.039981756719764],
                 [13, 0.038591534492843],
                 [14, 0.040670204402881],
                 [15, 0.036175997332008],
                 [16, 0.035507621351634],
                 [17, 0.033465181955469],
                 [18, 0.034212919400846],
                 [19, 0.033742936962832],
                 [20, 0.034053276266307],
                 [21, 0.03219543866338],
                 [22, 0.033189153177249],
                 [23, 0.03413387263725],
                 [24, 0.03059879185141],
                 [25, 0.030695859903176],
                 [26, 0.031359233283107],
                 [27, 0.03070709665186],
                 [28, 0.029417139620808],
                 [29, 0.02708770731278]]

ae_data = pd.DataFrame(ae_data_array, columns=['Latent size', 'Information loss']).set_index('Latent size')


def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png", dpi=300)


def run(data, file_name):
    plt.plot(data)

    plt.xlabel(data.index.name)
    plt.ylabel(data.columns[0])

    plt.axvline(x=14.5, linewidth=1, color='grey', linestyle=':')
    plt.axvline(x=21.5, linewidth=1, color='grey', linestyle=':')

    save_plot(os.path.join("output", "size_exploration", file_name))

    plt.close()


run(wv_data, "wv-size-exploration")
run(ae_data, "ae-size-exploration")
