import math
import os

import pandas as pd
import sklearn
from sklearn.cluster import KMeans

NO_REPEATS = 1000

imputed = pd.read_csv("../data-preprocessing/output/imputed.csv", index_col='LOB_GIATA')


def build_orig_clustering(n_clusters: int):
    best_score = 0
    best_labels = None

    for i in range(NO_REPEATS):
        if i % 100 == 0:
            print(f"Round {i}")

        k_means = KMeans(n_clusters=n_clusters)
        cluster_labels = k_means.fit_predict(imputed)
        silhouette_avg = sklearn.metrics.silhouette_score(imputed, cluster_labels)

        if silhouette_avg > best_score:
            print(f"Round {i}, New record silhouette average {silhouette_avg}")

            best_labels = cluster_labels
            best_score = silhouette_avg

    return best_labels


def run(n_clusters: int):
    if os.path.isfile(f"output/orig_cluster/{n_clusters}_clustering.csv"):
        return

    orig_clustering = build_orig_clustering(n_clusters)

    clustering = pd.DataFrame(index=imputed.index)
    clustering.index.name = "LOB_GIATA"
    clustering['orig'] = orig_clustering

    clustering.to_csv(f"output/orig_cluster/{n_clusters}_clustering.csv")


os.makedirs("output/orig_cluster", exist_ok=True)
for n in range(2, 21 + 1):
    run(n)
