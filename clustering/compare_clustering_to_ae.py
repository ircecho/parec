import pandas as pd

import clusim.clusim.clustering as clu
import clusim.clusim.sim as sim


def run(n_clusters: int, distance: pd.DataFrame):
    clustering = pd.read_csv(f"output/new_cluster/{n_clusters}_clustering.csv", index_col='LOB_GIATA')

    elm2clu_dict = {ind: [val] for ind, val in clustering['ae'].iteritems()}
    clusim_orig = clu.Clustering(elm2clu_dict=elm2clu_dict)

    for tp in clustering.columns:
        elm2clu_dict = {ind: [val] for ind, val in clustering[tp].iteritems()}
        clusim_clustering = clu.Clustering(elm2clu_dict=elm2clu_dict)

        adj_mi = sim.adj_mi(clusim_orig, clusim_clustering)
        distance.at[tp, n_clusters] = adj_mi

        print(f"{n_clusters} {tp} {adj_mi}")


dist = pd.DataFrame()
dist.index.name = 'tp'

for n in [7, 14, 21]:
    run(n, dist)

print(dist)
dist.to_csv("/tmp/clustering_vs_ae.csv")