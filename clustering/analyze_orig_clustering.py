import os

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import gridspec
import colorsys

def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png", dpi=300)

n_clusters = 11

clustering = pd.read_csv(f"output/orig_cluster/{n_clusters}_clustering.csv", index_col='LOB_GIATA')


plt.hist(clustering['orig'], bins=range(n_clusters + 1))  # density

plt.axes().set_xticks(list(map(lambda x: x + 0.5, range(n_clusters))))
plt.axes().set_xticklabels(range(1, n_clusters + 1))
plt.xlabel("Cluster")
plt.ylabel("Count")

save_plot(f"output/orig_cluster_histograms/histogram")

minbymax = pd.Series()

for n_clusters in range(2, 21 + 1):
    clustering = pd.read_csv(f"output/orig_cluster/{n_clusters}_clustering.csv", index_col='LOB_GIATA')

    orig = clustering['orig']
    values = pd.Series(dtype=int)
    for i in range(n_clusters):
        values.at[i] = len(orig[orig == i])

    print(f"\\num{{{n_clusters}}} clusters & \\num{{{values.min()}}} & \\num{{{round(values.mean())}}} & \\num{{{values.max()}}} \\\\")
    minbymax.at[n_clusters] = values.max()/values.min()

print(f"Average max/min: {round(minbymax.mean())}")

