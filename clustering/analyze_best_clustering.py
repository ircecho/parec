import math
import os
import statistics

import cartopy.crs as ccrs
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sklearn.metrics
from cartopy.feature import NaturalEarthFeature
from cartopy.mpl.geoaxes import GeoAxes
from matplotlib import pylab

hotel_data = pd.read_csv('../data-preprocessing/output/hotel-information.csv', index_col='LOB_GIATA')
coor_map = {ind: [val['Longitude'], val['Latitude']] for ind, val in hotel_data.iterrows()}

bb_80 = []


def set_bounding_boxes():
    global bb_80
    items = list(coor_map.values())

    while len(items) > len(hotel_data) * 0.8:
        x_mode = statistics.mode(map(lambda x: x[0], items))
        y_mode = statistics.mode(map(lambda x: x[1], items))

        max_index = max(range(len(items)),
                        key=lambda i: math.pow(items[i][0] - x_mode, 2) + math.pow(items[i][1] - y_mode, 2))
        del items[max_index]

    bb_80 = [min(map(lambda x: x[0], items)),
             max(map(lambda x: x[0], items)),
             min(map(lambda x: x[1], items)),
             max(map(lambda x: x[1], items))]


set_bounding_boxes()

print(bb_80)

imputed = pd.read_csv("../data-preprocessing/output/imputed.csv", index_col='LOB_GIATA')


def load_representations():
    li = [('original', imputed)]

    for f in sorted(os.listdir("representations")):
        t = os.path.splitext(f)[0]
        representation = pd.read_csv("representations/" + f, index_col='LOB_GIATA')
        li.append((t, representation))

    return li


files = load_representations()

def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    # plt.savefig(filename + ".pdf")
    # plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png", dpi=300)


def plot(data: pd.Series, filename: str):
    ax: GeoAxes = plt.axes(projection=ccrs.Robinson())
    ax.coastlines(resolution='10m', linewidth=0.5)

    ax.set_global()

    cm = pylab.get_cmap('tab20')

    handles = [mpatches.Patch(color=cm(1. * i / n_clusters), label=str(i)) for i in sorted(set(data.to_list()))]

    plt.legend(handles=handles)

    for ind, val in data.iteritems():
        if ind in coor_map:
            plt.scatter(coor_map[ind][0], coor_map[ind][1], color=cm(1. * val / n_clusters), marker='.',
                        transform=ccrs.Geodetic(), zorder=10, s=1)
    save_plot(f"output/maps/{n_clusters}/{filename}")

    borders = NaturalEarthFeature('cultural', 'admin_0_boundary_lines_land', '10m', edgecolor='black', facecolor='none')
    ax.add_feature(borders, linewidth=0.1)

    ax.set_extent(bb_80)
    save_plot(f"output/maps/{n_clusters}/{filename}_80")

    plt.close()


for n_clusters in [7, 15]:
    clusterings = pd.read_csv(f'output/cluster/{n_clusters}_clustering_clumpped.csv', index_col='LOB_GIATA')

    representations = []
    clusim_clusterings = {}

    for tp, rep in files:
        cluster_labels = clusterings[tp].to_numpy()

        sample_silhouette_values = sklearn.metrics.silhouette_samples(rep, cluster_labels)

        y_lower = 10
        for k in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[cluster_labels == k]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = plt.nipy_spectral()  # float(i) / n_clusters
            plt.fill_betweenx(np.arange(y_lower, y_upper),
                              0, ith_cluster_silhouette_values,
                              alpha=0.7)  # facecolor=color, edgecolor=color,

            # Label the silhouette plots with their cluster numbers at the middle
            plt.text(-0.05, y_lower + 0.5 * size_cluster_i, str(k))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        plt.title("The silhouette plot for the various clusters.")
        plt.xlabel("The silhouette coefficient values")
        plt.ylabel("Cluster label")

        silhouette_avg = sklearn.metrics.silhouette_score(rep, cluster_labels)

        # The vertical line for average silhouette score of all the values
        plt.axvline(x=silhouette_avg, color="red", linestyle="--")

        plt.yticks([])  # Clear the yaxis labels / ticks
        plt.xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

        save_plot(f"output/silhouette_plot/{n_clusters}/{tp}")
        plt.close()

        plot(clusterings[tp], f"{tp}/all")

        for j in range(n_clusters):
            plot(clusterings[tp][clusterings[tp] == j], f"{tp}/{j}")
