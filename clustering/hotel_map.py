import math
import os
import statistics

import cartopy.crs as ccrs
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sklearn.metrics
from cartopy.feature import NaturalEarthFeature
from cartopy.mpl.geoaxes import GeoAxes
from matplotlib import pylab

hotel_data = pd.read_csv('../data-preprocessing/output/hotel-information.csv', index_col='LOB_GIATA')
coor_map = {ind: [val['Longitude'], val['Latitude']] for ind, val in hotel_data.iterrows()}

bb_80 = []


def set_bounding_boxes():
    global bb_80
    items = list(coor_map.values())

    while len(items) > len(hotel_data) * 0.8:
        x_mode = statistics.mode(map(lambda x: x[0], items))
        y_mode = statistics.mode(map(lambda x: x[1], items))

        max_index = max(range(len(items)),
                        key=lambda i: math.pow(items[i][0] - x_mode, 2) + math.pow(items[i][1] - y_mode, 2))
        del items[max_index]

    bb_80 = [min(map(lambda x: x[0], items)),
             max(map(lambda x: x[0], items)),
             min(map(lambda x: x[1], items)),
             max(map(lambda x: x[1], items))]


set_bounding_boxes()

print(bb_80)

imputed = pd.read_csv("../data-preprocessing/output/imputed.csv", index_col='LOB_GIATA')


def load_representations():
    li = [('original', imputed)]

    for f in sorted(os.listdir("representations")):
        t = os.path.splitext(f)[0]
        representation = pd.read_csv("representations/" + f, index_col='LOB_GIATA')
        li.append((t, representation))

    return li


files = load_representations()

def save_plot(filename: str):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    plt.savefig(filename + ".pdf")
    plt.savefig(filename + ".pgf")
    plt.savefig(filename + ".png", dpi=300)


def plot(filename: str):
    ax: GeoAxes = plt.axes(projection=ccrs.Robinson())
    ax.coastlines(resolution='10m', linewidth=0.5)

    ax.set_global()

    for ind, val in coor_map.items():
            plt.scatter(val[0], val[1], color='red', marker='.',
                        transform=ccrs.Geodetic(), zorder=10, s=1)
    save_plot(f"output/maps/{filename}")

    borders = NaturalEarthFeature('cultural', 'admin_0_boundary_lines_land', '10m', edgecolor='black', facecolor='none')
    ax.add_feature(borders, linewidth=0.1)

    ax.set_extent(bb_80)
    save_plot(f"output/maps/{filename}_80")

    plt.close()



plot('hotels_map')