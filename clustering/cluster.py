import math
import os

import numpy
import pandas as pd
from sklearn.cluster import KMeans

import clusim.clusim.clustering as clu
import clusim.clusim.sim as sim
import general

NO_REPEATS = 1000

SUFFIX = "ae-size-exploration"
# SUFFIX = "wv-size-exploration"
# SUFFIX = "base"

if SUFFIX == "base":
    FILES = general.load_representations("")
else:
    FILES = general.load_representations(SUFFIX)
DIST_PATH = f"output/new_cluster/{SUFFIX}_clustering_distance_adj_mi.csv"


def optimize_distance(n_clusters: int, rep: pd.DataFrame, clusim_orig: clu.Clustering):
    best_score = 0
    best_labels = None
    best_clusim = None

    for i in range(NO_REPEATS):
        if i % 100 == 0:
            print(f"Round {i}")

        k_means = KMeans(n_clusters=n_clusters)
        cluster_labels = k_means.fit_predict(rep)

        elm2clu_dict = {ind: [val] for ind, val in zip(rep.index, cluster_labels)}
        clusim_clustering = clu.Clustering(elm2clu_dict=elm2clu_dict)

        nmi = sim.nmi(clusim_orig, clusim_clustering)

        if nmi > best_score:
            print(f"Round {i}, New record NMI: {nmi}")

            best_labels = cluster_labels
            best_clusim = clusim_clustering
            best_score = nmi

    return best_labels, sim.adj_mi(clusim_orig, best_clusim)


def run(n_clusters: int, distance: pd.DataFrame):
    clustering = pd.read_csv(f"output/orig_cluster/{n_clusters}_clustering.csv", index_col='LOB_GIATA')

    elm2clu_dict = {ind: [val] for ind, val in clustering['orig'].iteritems()}
    clusim_orig = clu.Clustering(elm2clu_dict=elm2clu_dict)

    for tp, rep in FILES:
        if (tp in distance.index) and \
                (n_clusters in distance.columns) and \
                not numpy.isnan(distance.at[tp, n_clusters]):
            continue

        print("Optimizing " + tp + "; " + str(n_clusters) + " clusters")
        best_labels, best_distance = optimize_distance(n_clusters, rep, clusim_orig)

        clustering[tp] = best_labels
        distance.at[tp, n_clusters] = best_distance

        os.makedirs("output/new_cluster", exist_ok=True)
        clustering.to_csv(f"output/new_cluster/{SUFFIX}_{n_clusters}_clustering.csv")
        dist.to_csv(DIST_PATH)


if os.path.isfile(DIST_PATH):
    dist = pd.read_csv(DIST_PATH, index_col='tp')
    dist.rename(columns=int, inplace=True)
else:
    dist = pd.DataFrame()
    dist.index.name = 'tp'

for n in [11]:
    run(n, dist)
