import subprocess
import tempfile

import numpy as np
import pandas as pd

for n_clusters in [7, 15]:
    clustering = pd.read_csv(f"output/cluster/{n_clusters}_clustering.csv", index_col="LOB_GIATA")

    popfile: tempfile.NamedTemporaryFile = tempfile.NamedTemporaryFile(mode="wt", suffix='.popfile', delete=False)

    for col in clustering:
        for ind, cl in clustering[col].iteritems():
            sparse = ['0'] * n_clusters
            sparse[cl] = '1'
            popfile.write(f"{ind} {' '.join(sparse)} 1\n")

        popfile.write("\n")

    popfile.close()

    outfile: tempfile.NamedTemporaryFile = tempfile.NamedTemporaryFile(mode="wt", suffix='.outfile', delete=False)
    outfile.close()

    miscfile: tempfile.NamedTemporaryFile = tempfile.NamedTemporaryFile(mode="wt", suffix='.miscfile', delete=False)
    miscfile.close()

    perm_datafile: tempfile.NamedTemporaryFile = tempfile.NamedTemporaryFile(mode="wt", suffix='.perm_datafile',
                                                                             delete=False)
    perm_datafile.close()

    paramfile: tempfile.NamedTemporaryFile = tempfile.NamedTemporaryFile(mode="wt", suffix='.paramfile', delete=False)
    paramfile.write("DATATYPE 1\n")
    # INDFILE =
    paramfile.write(f"POPFILE {popfile.name}\n")
    paramfile.write(f"OUTFILE {outfile.name}\n")
    paramfile.write(f"MISCFILE {miscfile.name}\n")
    paramfile.write(f"K {n_clusters}\n")
    paramfile.write(f"C {len(clustering.index)}\n")
    paramfile.write(f"R {len(clustering.columns)}\n")
    paramfile.write(f"M {3}\n")
    paramfile.write(f"W {0}\n")
    paramfile.write(f"S {2}\n")
    paramfile.write(f"GREEDY_OPTION {2}\n")
    paramfile.write(f"REPEATS {20_000}\n")
    paramfile.write(f"OVERRIDE_WARNINGS {0}\n")
    paramfile.write(f"ORDER_BY_RUN {0}\n")
    paramfile.write(f"PRINT_EVERY_PERM {0}\n")
    paramfile.write(f"PRINT_RANDOM_INPUTORDER {0}\n")
    paramfile.write(f"PRINT_PERMUTED_DATA {1}\n")
    paramfile.write(f"PERMUTED_DATAFILE {perm_datafile.name}\n")
    # PERMUTATIONFILE =

    paramfile.close()
    print(paramfile.name)

    subprocess.run(["CLUMPP_Linux64.1.1.2/CLUMPP", paramfile.name])

    results = pd.read_csv(perm_datafile.name, sep=':?\\s+', header=None, engine='python')

    split = np.split(results, len(results.index) / len(clustering.index))

    for i, col in enumerate(clustering):
        for ind, row in split[i].iterrows():
            clustering.at[int(row[0]), col] = sum(map(lambda x: (x[0] - 1) * x[1], row[1:-1].iteritems()))

    clustering.to_csv(f"output/cluster/{n_clusters}_clustering_clumpped.csv")
