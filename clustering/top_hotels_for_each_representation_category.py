import os

import pandas as pd

n: int = 5

hotel_data = pd.read_csv('../data-preprocessing/output/hotel-information.csv', index_col='LOB_GIATA')


def load_representations():
    li = []

    for f in sorted(os.listdir("representations")):
        t = os.path.splitext(f)[0]
        representation = pd.read_csv("representations/" + f, index_col='LOB_GIATA')
        li.append((t, representation))

    return li


files = load_representations()


def print_hotels(hotels: pd.Series):
    for ind, c_val in hotels.iteritems():
        if ind in hotel_data.index:
            hotel = hotel_data.loc[ind].tolist()
        else:
            hotel = "unknown"

        f.write(str(ind) + " " + ("%.2f" % c_val) + " " + str(hotel) + "\n")


for tp, rep in files:
    os.makedirs("output/top-5", exist_ok=True)
    with open("output/top-5/" + tp + ".txt", "w") as f:

        for col in rep.columns:
            f.write("Column " + col + "\n")

            f.write("Top " + str(n) + "\n")
            top_n = rep[col].sort_values(ascending=False).head(n)

            print_hotels(top_n)

            f.write("Bottom " + str(n) + "\n")
            low_n = rep[col].sort_values(ascending=True).head(n)

            print_hotels(low_n)
            f.write("\n")
