import os

import pandas as pd


def load_representations(path_suffix: str = ""):
    li = []

    directory = os.path.join("representations", path_suffix)

    for f in sorted(os.listdir(directory)):
        file_path = os.path.join(directory, f)

        if not os.path.isfile(file_path):
            continue

        t = os.path.splitext(f)[0]
        representation = pd.read_csv(file_path, index_col='LOB_GIATA')
        li.append((t, representation))

    return li
